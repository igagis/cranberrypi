EESchema Schematic File Version 4
LIBS:mainboard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 13 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR0154
U 1 1 5E6DC0F7
P 3150 4100
F 0 "#PWR0154" H 3150 3850 50  0001 C CNN
F 1 "GND" H 3155 3927 50  0000 C CNN
F 2 "" H 3150 4100 50  0001 C CNN
F 3 "" H 3150 4100 50  0001 C CNN
	1    3150 4100
	1    0    0    -1  
$EndComp
$Comp
L wifi:rtl8189ftv U14
U 1 1 5E6FD350
P 4300 3050
F 0 "U14" H 4300 4075 50  0000 C CNN
F 1 "rtl8189ftv" H 4300 3984 50  0000 C CNN
F 2 "Package_DFN_QFN:QFN-24-1EP_4x4mm_P0.5mm_EP2.6x2.6mm_ThermalVias" H 3950 3500 50  0001 C CNN
F 3 "" H 3950 3500 50  0001 C CNN
	1    4300 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4100 3150 3850
Wire Wire Line
	3150 3850 3350 3850
Wire Wire Line
	3350 3650 3200 3650
Wire Wire Line
	3200 3650 3200 3550
Wire Wire Line
	3200 2950 3350 2950
Wire Wire Line
	3350 3050 3200 3050
Connection ~ 3200 3050
Wire Wire Line
	3200 3050 3200 2950
Wire Wire Line
	3350 3150 3200 3150
Connection ~ 3200 3150
Wire Wire Line
	3200 3150 3200 3050
Wire Wire Line
	3350 3250 3200 3250
Connection ~ 3200 3250
Wire Wire Line
	3200 3250 3200 3150
Wire Wire Line
	3350 3350 3200 3350
Connection ~ 3200 3350
Wire Wire Line
	3200 3350 3200 3250
Wire Wire Line
	3350 3450 3200 3450
Connection ~ 3200 3450
Wire Wire Line
	3200 3450 3200 3350
Wire Wire Line
	3350 3550 3200 3550
Connection ~ 3200 3550
Wire Wire Line
	3200 3550 3200 3450
Wire Wire Line
	3200 2950 1350 2950
Connection ~ 3200 2950
$Comp
L power:+3V3 #PWR0142
U 1 1 5E705622
P 1250 2950
F 0 "#PWR0142" H 1250 2800 50  0001 C CNN
F 1 "+3V3" V 1265 3078 50  0000 L CNN
F 2 "" H 1250 2950 50  0001 C CNN
F 3 "" H 1250 2950 50  0001 C CNN
	1    1250 2950
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C127
U 1 1 5E70595E
P 2800 3650
F 0 "C127" V 2750 3400 50  0000 L CNN
F 1 "0.1uF" V 2850 3400 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2838 3500 50  0001 C CNN
F 3 "~" H 2800 3650 50  0001 C CNN
	1    2800 3650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0152
U 1 1 5E705DCD
P 2600 3650
F 0 "#PWR0152" H 2600 3400 50  0001 C CNN
F 1 "GND" V 2605 3522 50  0000 R CNN
F 2 "" H 2600 3650 50  0001 C CNN
F 3 "" H 2600 3650 50  0001 C CNN
	1    2600 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	2600 3650 2650 3650
Wire Wire Line
	2950 3650 3200 3650
Connection ~ 3200 3650
$Comp
L Device:C C125
U 1 1 5E707A3E
P 2650 3450
F 0 "C125" V 2600 3200 50  0000 L CNN
F 1 "0.1uF" V 2700 3200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2688 3300 50  0001 C CNN
F 3 "~" H 2650 3450 50  0001 C CNN
	1    2650 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C123
U 1 1 5E707A8A
P 2250 3350
F 0 "C123" V 2200 3100 50  0000 L CNN
F 1 "0.1uF" V 2300 3100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2288 3200 50  0001 C CNN
F 3 "~" H 2250 3350 50  0001 C CNN
	1    2250 3350
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C124
U 1 1 5E707AB6
P 2550 3150
F 0 "C124" V 2500 2900 50  0000 L CNN
F 1 "0.1uF" V 2600 2900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2588 3000 50  0001 C CNN
F 3 "~" H 2550 3150 50  0001 C CNN
	1    2550 3150
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C120
U 1 1 5E707AF2
P 1350 3200
F 0 "C120" V 1300 2950 50  0000 L CNN
F 1 "0.1uF" V 1400 2950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1388 3050 50  0001 C CNN
F 3 "~" H 1350 3200 50  0001 C CNN
	1    1350 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 3050 1350 2950
Connection ~ 1350 2950
Wire Wire Line
	1350 2950 1250 2950
Wire Wire Line
	2700 3150 3200 3150
Wire Wire Line
	2400 3350 3200 3350
Wire Wire Line
	2800 3450 3200 3450
$Comp
L power:GND #PWR0149
U 1 1 5E708EDD
P 2050 3350
F 0 "#PWR0149" H 2050 3100 50  0001 C CNN
F 1 "GND" V 2055 3222 50  0000 R CNN
F 2 "" H 2050 3350 50  0001 C CNN
F 3 "" H 2050 3350 50  0001 C CNN
	1    2050 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	2050 3350 2100 3350
$Comp
L power:GND #PWR0150
U 1 1 5E7094E8
P 2050 3450
F 0 "#PWR0150" H 2050 3200 50  0001 C CNN
F 1 "GND" V 2055 3322 50  0000 R CNN
F 2 "" H 2050 3450 50  0001 C CNN
F 3 "" H 2050 3450 50  0001 C CNN
	1    2050 3450
	0    1    1    0   
$EndComp
Wire Wire Line
	2050 3450 2500 3450
$Comp
L power:GND #PWR0148
U 1 1 5E70A820
P 2050 3150
F 0 "#PWR0148" H 2050 2900 50  0001 C CNN
F 1 "GND" V 2055 3022 50  0000 R CNN
F 2 "" H 2050 3150 50  0001 C CNN
F 3 "" H 2050 3150 50  0001 C CNN
	1    2050 3150
	0    1    1    0   
$EndComp
Wire Wire Line
	2050 3150 2400 3150
$Comp
L power:GND #PWR0144
U 1 1 5E70C433
P 1350 3550
F 0 "#PWR0144" H 1350 3300 50  0001 C CNN
F 1 "GND" H 1200 3500 50  0000 C CNN
F 2 "" H 1350 3550 50  0001 C CNN
F 3 "" H 1350 3550 50  0001 C CNN
	1    1350 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 3550 1350 3350
$Comp
L Device:Crystal_GND24 Y?
U 1 1 5E70E3B2
P 6750 2900
AR Path="/5E6A4B6B/5E70E3B2" Ref="Y?"  Part="1" 
AR Path="/5E68FE1D/5E70E3B2" Ref="Y?"  Part="1" 
AR Path="/5E6DC0BF/5E70E3B2" Ref="Y4"  Part="1" 
F 0 "Y4" V 6600 3050 50  0000 L CNN
F 1 "25MHz 10ppm" V 6900 3000 50  0000 L CNN
F 2 "Oscillator:Oscillator_SMD_EuroQuartz_XO32-4Pin_3.2x2.5mm" H 6750 2900 50  0001 C CNN
F 3 "~" H 6750 2900 50  0001 C CNN
F 4 "X322525MOB4SI" V 7000 3250 50  0000 C CNN "part"
	1    6750 2900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5E70E3B9
P 7050 3200
AR Path="/5E6A4B6B/5E70E3B9" Ref="C?"  Part="1" 
AR Path="/5E68FE1D/5E70E3B9" Ref="C?"  Part="1" 
AR Path="/5E6DC0BF/5E70E3B9" Ref="C129"  Part="1" 
F 0 "C129" V 6798 3200 50  0000 C CNN
F 1 "22pF" V 6889 3200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7088 3050 50  0001 C CNN
F 3 "~" H 7050 3200 50  0001 C CNN
	1    7050 3200
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5E70E3C0
P 7050 2600
AR Path="/5E6A4B6B/5E70E3C0" Ref="C?"  Part="1" 
AR Path="/5E68FE1D/5E70E3C0" Ref="C?"  Part="1" 
AR Path="/5E6DC0BF/5E70E3C0" Ref="C128"  Part="1" 
F 0 "C128" V 7150 2450 50  0000 C CNN
F 1 "22pF" V 7000 2450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7088 2450 50  0001 C CNN
F 3 "~" H 7050 2600 50  0001 C CNN
	1    7050 2600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E70E3C7
P 7350 2900
AR Path="/5E6A4B6B/5E70E3C7" Ref="#PWR?"  Part="1" 
AR Path="/5E68FE1D/5E70E3C7" Ref="#PWR?"  Part="1" 
AR Path="/5E6DC0BF/5E70E3C7" Ref="#PWR0157"  Part="1" 
F 0 "#PWR0157" H 7350 2650 50  0001 C CNN
F 1 "GND" V 7355 2772 50  0000 R CNN
F 2 "" H 7350 2900 50  0001 C CNN
F 3 "" H 7350 2900 50  0001 C CNN
	1    7350 2900
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E70E3CD
P 6550 2900
AR Path="/5E6A4B6B/5E70E3CD" Ref="#PWR?"  Part="1" 
AR Path="/5E68FE1D/5E70E3CD" Ref="#PWR?"  Part="1" 
AR Path="/5E6DC0BF/5E70E3CD" Ref="#PWR0156"  Part="1" 
F 0 "#PWR0156" H 6550 2650 50  0001 C CNN
F 1 "GND" H 6650 2750 50  0000 R CNN
F 2 "" H 6550 2900 50  0001 C CNN
F 3 "" H 6550 2900 50  0001 C CNN
	1    6550 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	7200 3200 7350 3200
Wire Wire Line
	7350 3200 7350 2900
Wire Wire Line
	7350 2600 7200 2600
Connection ~ 7350 2900
Wire Wire Line
	7350 2900 7350 2600
Wire Wire Line
	6950 2900 7350 2900
Wire Wire Line
	6900 3200 6750 3200
Wire Wire Line
	6750 2750 6750 2600
Connection ~ 6750 2600
Wire Wire Line
	6750 2600 6900 2600
Wire Wire Line
	6750 3050 6750 3200
Connection ~ 6750 3200
Wire Wire Line
	5250 2600 6750 2600
Wire Wire Line
	6050 3200 6750 3200
Wire Wire Line
	6050 3200 6050 2700
Wire Wire Line
	6050 2700 5250 2700
$Comp
L power:+3V3 #PWR0151
U 1 1 5E712641
P 2550 1550
F 0 "#PWR0151" H 2550 1400 50  0001 C CNN
F 1 "+3V3" H 2565 1723 50  0000 C CNN
F 2 "" H 2550 1550 50  0001 C CNN
F 3 "" H 2550 1550 50  0001 C CNN
	1    2550 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 2250 1650 2250
Text GLabel 1450 2250 0    50   Input ~ 0
wifi_sd_clk
Wire Wire Line
	3350 2350 3050 2350
Wire Wire Line
	3350 2450 2800 2450
Wire Wire Line
	3350 2550 2550 2550
Wire Wire Line
	3350 2650 2300 2650
Wire Wire Line
	3350 2750 2050 2750
Text GLabel 1450 2350 0    50   Input ~ 0
wifi_sd_cmd
Text GLabel 1450 2450 0    50   Input ~ 0
wifi_sd_d0
Text GLabel 1450 2550 0    50   Input ~ 0
wifi_sd_d1
Text GLabel 1450 2650 0    50   Input ~ 0
wifi_sd_d2
Text GLabel 1450 2750 0    50   Input ~ 0
wifi_sd_d3
$Comp
L Device:R R96
U 1 1 5E71923D
P 3050 1950
F 0 "R96" V 2950 1900 50  0000 L CNN
F 1 "10k" V 3050 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2980 1950 50  0001 C CNN
F 3 "~" H 3050 1950 50  0001 C CNN
	1    3050 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 2100 3050 2350
Connection ~ 3050 2350
Wire Wire Line
	3050 2350 1450 2350
$Comp
L Device:R R95
U 1 1 5E71B52B
P 2800 1950
F 0 "R95" V 2700 1900 50  0000 L CNN
F 1 "10k" V 2800 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2730 1950 50  0001 C CNN
F 3 "~" H 2800 1950 50  0001 C CNN
	1    2800 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R94
U 1 1 5E71B55D
P 2550 1950
F 0 "R94" V 2450 1900 50  0000 L CNN
F 1 "10k" V 2550 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2480 1950 50  0001 C CNN
F 3 "~" H 2550 1950 50  0001 C CNN
	1    2550 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R93
U 1 1 5E71B59F
P 2300 1950
F 0 "R93" V 2200 1900 50  0000 L CNN
F 1 "10k" V 2300 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2230 1950 50  0001 C CNN
F 3 "~" H 2300 1950 50  0001 C CNN
	1    2300 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R92
U 1 1 5E71B5D5
P 2050 1950
F 0 "R92" V 1950 1900 50  0000 L CNN
F 1 "10k" V 2050 1850 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1980 1950 50  0001 C CNN
F 3 "~" H 2050 1950 50  0001 C CNN
	1    2050 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 2100 2800 2450
Connection ~ 2800 2450
Wire Wire Line
	2800 2450 1450 2450
Wire Wire Line
	2550 2100 2550 2550
Connection ~ 2550 2550
Wire Wire Line
	2550 2550 1450 2550
Wire Wire Line
	2300 2100 2300 2650
Connection ~ 2300 2650
Wire Wire Line
	2300 2650 1450 2650
Wire Wire Line
	2050 2100 2050 2750
Connection ~ 2050 2750
Wire Wire Line
	2050 2750 1450 2750
Wire Wire Line
	2050 1800 2050 1550
Wire Wire Line
	2050 1550 2300 1550
Wire Wire Line
	3050 1550 3050 1800
Connection ~ 2550 1550
Wire Wire Line
	2550 1550 2800 1550
Wire Wire Line
	2800 1800 2800 1550
Connection ~ 2800 1550
Wire Wire Line
	2800 1550 3050 1550
Wire Wire Line
	2550 1800 2550 1550
Wire Wire Line
	2300 1800 2300 1550
Connection ~ 2300 1550
Wire Wire Line
	2300 1550 2550 1550
$Comp
L Device:C C122
U 1 1 5E729C03
P 1650 1800
F 0 "C122" V 1700 1850 50  0000 L CNN
F 1 "10pF" V 1600 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1688 1650 50  0001 C CNN
F 3 "~" H 1650 1800 50  0001 C CNN
F 4 "NC" V 1550 1650 50  0000 C CNN "info"
	1    1650 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0146
U 1 1 5E729C4F
P 1650 1400
F 0 "#PWR0146" H 1650 1150 50  0001 C CNN
F 1 "GND" H 1500 1350 50  0000 C CNN
F 2 "" H 1650 1400 50  0001 C CNN
F 3 "" H 1650 1400 50  0001 C CNN
	1    1650 1400
	-1   0    0    1   
$EndComp
Wire Wire Line
	1650 1400 1650 1650
Wire Wire Line
	1650 1950 1650 2250
Connection ~ 1650 2250
Wire Wire Line
	1650 2250 3350 2250
$Comp
L Device:Antenna_Chip AE1
U 1 1 5E73B8E6
P 10200 2150
F 0 "AE1" H 10379 2278 50  0000 L CNN
F 1 "Antenna_Chip" H 10379 2187 50  0000 L CNN
F 2 "rf_antenna:rainsun_an9520_245" H 10100 2325 50  0001 C CNN
F 3 "~" H 10100 2325 50  0001 C CNN
	1    10200 2150
	1    0    0    -1  
$EndComp
NoConn ~ 10300 2250
$Comp
L power:+3V3 #PWR0155
U 1 1 5E759CE9
P 5550 2800
F 0 "#PWR0155" H 5550 2650 50  0001 C CNN
F 1 "+3V3" V 5565 2928 50  0000 L CNN
F 2 "" H 5550 2800 50  0001 C CNN
F 3 "" H 5550 2800 50  0001 C CNN
	1    5550 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	5550 2800 5250 2800
NoConn ~ 5250 3200
Text GLabel 5500 3800 3    50   Input ~ 0
wifi_wake
$Comp
L Device:R R97
U 1 1 5E76C238
P 5500 3500
F 0 "R97" V 5400 3450 50  0000 L CNN
F 1 "33" V 5500 3450 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5430 3500 50  0001 C CNN
F 3 "~" H 5500 3500 50  0001 C CNN
	1    5500 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3650 5500 3800
Wire Wire Line
	5500 3350 5500 3100
Wire Wire Line
	5500 3100 5250 3100
Text Notes 6100 2200 0    50   ~ 0
50ohm microstrip
Wire Wire Line
	5250 2250 10100 2250
Text Notes 7750 2200 0    50   ~ 0
go without compensation network\nsince I have no network analyzer to tune it anyway
NoConn ~ 5250 2900
NoConn ~ 5250 3000
NoConn ~ 3350 3750
Text Notes 1050 4250 0    50   ~ 0
for vd12d looks like there is LDO\nregulator inside RTL8189FTV for 1.2V
Text Notes 7500 6900 0    118  ~ 24
wifi
$EndSCHEMATC

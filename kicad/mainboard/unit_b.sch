EESchema Schematic File Version 4
LIBS:mainboard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L cpu_ingenic:jz4780 U1
U 2 1 5E6CA915
P 5650 3600
F 0 "U1" H 5600 5865 50  0000 C CNN
F 1 "jz4780" H 5600 5774 50  0000 C CNN
F 2 "ingenic:jz4780" H 4700 4950 50  0001 C CNN
F 3 "ftp://ftp.ingenic.com/SOC/JZ4780/JZ4780_DS.PDF" H 5800 5900 50  0001 C CNN
	2    5650 3600
	1    0    0    -1  
$EndComp
Text Notes 7200 6950 0    79   ~ 16
cpu unit b\nuart4
$Comp
L Connector_Generic:Conn_01x04 J5
U 1 1 5E6CCC95
P 7950 3650
F 0 "J5" H 8029 3642 50  0000 L CNN
F 1 "uart4" H 8029 3551 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7950 3650 50  0001 C CNN
F 3 "~" H 7950 3650 50  0001 C CNN
	1    7950 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 3550 7750 3250
$Comp
L power:+3V3 #PWR0111
U 1 1 5E6CCD67
P 7750 3200
F 0 "#PWR0111" H 7750 3050 50  0001 C CNN
F 1 "+3V3" H 7765 3373 50  0000 C CNN
F 2 "" H 7750 3200 50  0001 C CNN
F 3 "" H 7750 3200 50  0001 C CNN
	1    7750 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5E6CCDB9
P 7750 4000
F 0 "#PWR0112" H 7750 3750 50  0001 C CNN
F 1 "GND" H 7755 3827 50  0000 C CNN
F 2 "" H 7750 4000 50  0001 C CNN
F 3 "" H 7750 4000 50  0001 C CNN
	1    7750 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 4000 7750 3850
Wire Wire Line
	7750 3650 7350 3650
Wire Wire Line
	7350 3650 7350 3300
Wire Wire Line
	7350 3300 7050 3300
Wire Wire Line
	7750 3750 7550 3750
Wire Wire Line
	7550 3750 7550 4300
Wire Wire Line
	7550 4300 7050 4300
$Comp
L Device:R R57
U 1 1 5E6CCE98
P 7550 3400
F 0 "R57" V 7650 3300 50  0000 L CNN
F 1 "47k" V 7550 3300 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7480 3400 50  0001 C CNN
F 3 "~" H 7550 3400 50  0001 C CNN
	1    7550 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 3550 7550 3750
Connection ~ 7550 3750
Wire Wire Line
	7550 3250 7750 3250
Connection ~ 7750 3250
Wire Wire Line
	7750 3250 7750 3200
Wire Wire Line
	4150 3700 3800 3700
Text GLabel 3800 3700 0    50   Input ~ 0
ethernet_rd_n
Wire Wire Line
	4150 3800 3800 3800
Text GLabel 3800 3800 0    50   Input ~ 0
ethernet_we_n
Wire Wire Line
	4150 4700 3800 4700
Text GLabel 3800 4700 0    50   Input ~ 0
ethernet_cs_n
Wire Wire Line
	4150 3200 3800 3200
Text GLabel 3800 3200 0    50   Input ~ 0
sa1_ale
Text GLabel 3800 2300 0    50   Input ~ 0
sd0
Text GLabel 3800 2400 0    50   Input ~ 0
sd1
Text GLabel 3800 2500 0    50   Input ~ 0
sd2
Text GLabel 3800 2600 0    50   Input ~ 0
sd3
Text GLabel 3800 2700 0    50   Input ~ 0
sd4
Text GLabel 3800 2800 0    50   Input ~ 0
sd5
Text GLabel 3800 2900 0    50   Input ~ 0
sd6
Text GLabel 3800 3000 0    50   Input ~ 0
sd7
Wire Wire Line
	3800 3000 4150 3000
Wire Wire Line
	4150 2900 3800 2900
Wire Wire Line
	3800 2800 4150 2800
Wire Wire Line
	4150 2700 3800 2700
Wire Wire Line
	3800 2600 4150 2600
Wire Wire Line
	4150 2500 3800 2500
Wire Wire Line
	3800 2400 4150 2400
Wire Wire Line
	4150 2300 3800 2300
Text GLabel 7450 2300 2    50   Input ~ 0
gpio_pc0
Wire Wire Line
	7450 2300 7050 2300
Text GLabel 7450 2400 2    50   Input ~ 0
gpio_pc1
Wire Wire Line
	7450 2400 7050 2400
NoConn ~ 4150 3100
NoConn ~ 4150 3300
NoConn ~ 4150 3400
NoConn ~ 4150 3500
NoConn ~ 4150 3600
NoConn ~ 4150 3900
NoConn ~ 4150 4000
NoConn ~ 4150 4100
NoConn ~ 4150 4200
NoConn ~ 4150 4300
NoConn ~ 4150 4400
NoConn ~ 4150 4500
NoConn ~ 4150 4600
NoConn ~ 4150 4800
$Comp
L Device:R R56
U 1 1 5E958C6E
P 2850 4600
F 0 "R56" V 2950 4500 50  0000 L CNN
F 1 "10k" V 2850 4500 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2780 4600 50  0001 C CNN
F 3 "~" H 2850 4600 50  0001 C CNN
	1    2850 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 4750 2850 4900
Wire Wire Line
	2850 4900 4150 4900
$Comp
L power:+3V3 #PWR0110
U 1 1 5E959166
P 2850 4300
F 0 "#PWR0110" H 2850 4150 50  0001 C CNN
F 1 "+3V3" H 2865 4473 50  0000 C CNN
F 2 "" H 2850 4300 50  0001 C CNN
F 3 "" H 2850 4300 50  0001 C CNN
	1    2850 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 4300 2850 4450
Text Notes 1800 5000 0    50   ~ 0
in Creator ci20 this pin\nis used for nIRQ from PMU
NoConn ~ 4150 5000
NoConn ~ 7050 2500
NoConn ~ 7050 2600
NoConn ~ 7050 2700
NoConn ~ 7050 2800
NoConn ~ 7050 2900
NoConn ~ 7050 3000
NoConn ~ 7050 3100
NoConn ~ 7050 3200
NoConn ~ 7050 3400
NoConn ~ 7050 3500
NoConn ~ 7050 3600
NoConn ~ 7050 3700
NoConn ~ 7050 3800
NoConn ~ 7050 3900
NoConn ~ 7050 4000
NoConn ~ 7050 4100
NoConn ~ 7050 4200
NoConn ~ 7050 4400
NoConn ~ 7050 4500
NoConn ~ 7050 4600
NoConn ~ 7050 4700
NoConn ~ 7050 4800
NoConn ~ 7050 4900
NoConn ~ 7050 5000
$EndSCHEMATC

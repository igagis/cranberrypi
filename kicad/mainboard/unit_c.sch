EESchema Schematic File Version 4
LIBS:mainboard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L cpu_ingenic:jz4780 U1
U 3 1 5E6EF367
P 5550 3650
F 0 "U1" H 5575 5915 50  0000 C CNN
F 1 "jz4780" H 5575 5824 50  0000 C CNN
F 2 "ingenic:jz4780" H 4600 5000 50  0001 C CNN
F 3 "ftp://ftp.ingenic.com/SOC/JZ4780/JZ4780_DS.PDF" H 5700 5950 50  0001 C CNN
	3    5550 3650
	1    0    0    -1  
$EndComp
Text Notes 7250 6900 0    118  ~ 24
cpu unit c
Wire Wire Line
	7100 3450 7550 3450
Wire Wire Line
	7550 3550 7100 3550
Wire Wire Line
	7100 3650 7550 3650
Wire Wire Line
	7550 3750 7100 3750
Wire Wire Line
	7100 3850 7550 3850
Wire Wire Line
	7550 3950 7100 3950
Text GLabel 7550 3450 2    50   Input ~ 0
msc0_clk
Text GLabel 7550 3550 2    50   Input ~ 0
msc0_cmd
Text GLabel 7550 3650 2    50   Input ~ 0
msc0_d0
Text GLabel 7550 3750 2    50   Input ~ 0
msc0_d1
Text GLabel 7550 3850 2    50   Input ~ 0
msc0_d2
Text GLabel 7550 3950 2    50   Input ~ 0
msc0_d3
Wire Wire Line
	7100 4550 7550 4550
Text GLabel 7550 4550 2    50   Input ~ 0
ethernet_int
Wire Wire Line
	7100 2050 7950 2050
Wire Wire Line
	7100 2150 7950 2150
Wire Wire Line
	7100 2250 7950 2250
Wire Wire Line
	7100 2350 7950 2350
Wire Wire Line
	7100 2450 7950 2450
Wire Wire Line
	7100 2550 7950 2550
Text GLabel 7950 2050 2    50   Input ~ 0
wifi_sd_d0
Text GLabel 7950 2150 2    50   Input ~ 0
wifi_sd_d1
Text GLabel 7950 2250 2    50   Input ~ 0
wifi_sd_d2
Text GLabel 7950 2350 2    50   Input ~ 0
wifi_sd_d3
Text GLabel 7950 2450 2    50   Input ~ 0
wifi_sd_clk
Text GLabel 7950 2550 2    50   Input ~ 0
wifi_sd_cmd
Text GLabel 7550 5550 2    50   Input ~ 0
wifi_wake
Wire Wire Line
	7550 5550 7100 5550
NoConn ~ 4050 1650
NoConn ~ 4050 1750
NoConn ~ 4050 1850
NoConn ~ 4050 1950
NoConn ~ 4050 2050
NoConn ~ 4050 2150
NoConn ~ 4050 2250
NoConn ~ 4050 2350
NoConn ~ 4050 2550
NoConn ~ 4050 2650
NoConn ~ 4050 2850
NoConn ~ 4050 2950
NoConn ~ 4050 3050
NoConn ~ 4050 3150
NoConn ~ 4050 3250
NoConn ~ 4050 3350
NoConn ~ 4050 3450
NoConn ~ 4050 3550
NoConn ~ 4050 3650
NoConn ~ 4050 3750
NoConn ~ 4050 3850
NoConn ~ 4050 3950
NoConn ~ 4050 4050
NoConn ~ 4050 4150
NoConn ~ 4050 4250
NoConn ~ 4050 4350
NoConn ~ 4050 4450
NoConn ~ 4050 4550
NoConn ~ 4050 4650
NoConn ~ 4050 4750
NoConn ~ 4050 4850
NoConn ~ 4050 4950
NoConn ~ 4050 5050
NoConn ~ 4050 5150
NoConn ~ 4050 5250
NoConn ~ 4050 5350
NoConn ~ 4050 5450
NoConn ~ 4050 5550
NoConn ~ 7100 1650
NoConn ~ 7100 1750
NoConn ~ 7100 1850
NoConn ~ 7100 1950
NoConn ~ 7100 2650
NoConn ~ 7100 2750
NoConn ~ 7100 2850
NoConn ~ 7100 2950
NoConn ~ 7100 3050
NoConn ~ 7100 3150
NoConn ~ 7100 3250
NoConn ~ 7100 3350
NoConn ~ 7100 4050
NoConn ~ 7100 4150
NoConn ~ 7100 4250
NoConn ~ 7100 4350
NoConn ~ 7100 4450
NoConn ~ 7100 4650
NoConn ~ 7100 4750
NoConn ~ 7100 4850
NoConn ~ 7100 4950
NoConn ~ 7100 5050
NoConn ~ 7100 5150
NoConn ~ 7100 5250
NoConn ~ 7100 5350
NoConn ~ 7100 5450
NoConn ~ 4050 2750
NoConn ~ 4050 2450
$EndSCHEMATC

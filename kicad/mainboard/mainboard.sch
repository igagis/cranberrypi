EESchema Schematic File Version 4
LIBS:mainboard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 700  800  1200 500 
U 5E623205
F0 "cpu unit a" 118
F1 "unit_a.sch" 118
$EndSheet
$Sheet
S 650  2000 2900 500 
U 5E63E74E
F0 "power management unit" 118
F1 "pmu.sch" 118
$EndSheet
Text Notes 7200 6900 0    157  ~ 31
main sheet
$Sheet
S 9100 800  1950 750 
U 5E6A4B6B
F0 "cpu unit e" 118
F1 "unit_e.sch" 118
$EndSheet
$Sheet
S 4050 2000 1400 500 
U 5E6A7361
F0 "sd card" 118
F1 "sd.sch" 118
$EndSheet
Text Notes 750  2400 0    118  ~ 24
pmu\npower connector
Text Notes 800  1200 0    118  ~ 24
cpu unit a\nddr
Text Notes 9150 1250 0    118  ~ 24
cpu unit e\n(hdmi), crystal, rtc
Text Notes 4250 2350 0    118  ~ 24
sd card\n
$Sheet
S 6050 2100 1650 500 
U 5E6A9EBD
F0 "hdmi" 118
F1 "hdmi.sch" 118
$EndSheet
Text Notes 6600 2450 0    118  ~ 24
hdmi
$Sheet
S 6650 800  2250 800 
U 5E65B7E5
F0 "cpu unit d" 118
F1 "unit_d.sch" 118
$EndSheet
Text Notes 6700 1450 0    118  ~ 24
cpu uint d\nuart3, power, jtag,\nboot_sel
$Sheet
S 2150 800  2150 500 
U 5E6C9F0E
F0 "cpu unit b" 118
F1 "unit_b.sch" 118
$EndSheet
Text Notes 2250 1200 0    118  ~ 24
cpu unit b\nuart4
$Sheet
S 4550 800  1800 500 
U 5E6EE54E
F0 "cpu unit c" 118
F1 "unit_c.sch" 118
$EndSheet
Text Notes 4600 1200 0    118  ~ 24
cpu unit c\n(msc0, wifi)
$Sheet
S 8350 2100 1250 500 
U 5E70A26F
F0 "usb" 118
F1 "usb.sch" 118
$EndSheet
Text Notes 8750 2450 0    118  ~ 24
usb
$Sheet
S 750  3050 1950 700 
U 5E68FE1D
F0 "ethernet" 118
F1 "net.sch" 118
$EndSheet
Text Notes 900  3450 0    118  ~ 24
ethernet
$Sheet
S 3350 3100 2100 600 
U 5E6DAC31
F0 "gpio 1" 118
F1 "gpio1.sch" 118
$EndSheet
Text Notes 3500 3450 0    118  ~ 24
gpio 1
$Sheet
S 5900 3100 1900 600 
U 5E6DC0BF
F0 "wifi" 118
F1 "wifi.sch" 118
$EndSheet
Text Notes 6100 3450 0    118  ~ 24
wifi
Text Notes 10700 7650 0    79   ~ 16
0.1
Text Notes 8350 7650 0    79   ~ 16
2020-04-06
$EndSCHEMATC

EESchema Schematic File Version 4
LIBS:mainboard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ddr:SK-hynix_H5TQ4G63CFR-RDC U?
U 1 1 5E62362D
P 5600 3450
AR Path="/5E62362D" Ref="U?"  Part="1" 
AR Path="/5E623205/5E62362D" Ref="U2"  Part="1" 
F 0 "U2" H 5600 5975 50  0000 C CNN
F 1 "SK-hynix_H5TQ4G63CFR-RDC" H 5600 5884 50  0000 C CNN
F 2 "package_bga:BGA-96_7.5x13.0mm_Layout2x3x16_P0.8mm_Ball0.4mm_Pad0.3mm" H 6000 4700 50  0001 C CNN
F 3 "" H 6000 4700 50  0001 C CNN
	1    5600 3450
	1    0    0    -1  
$EndComp
NoConn ~ 2600 2550
Text Label 2750 1250 2    50   ~ 0
a2
Text Label 2750 1350 2    50   ~ 0
a3
Text Label 2750 1450 2    50   ~ 0
a4
Text Label 2750 1550 2    50   ~ 0
a5
Text Label 2750 1650 2    50   ~ 0
a6
Text Label 2750 1750 2    50   ~ 0
a7
Text Label 2750 1850 2    50   ~ 0
a8
Text Label 2750 1950 2    50   ~ 0
a9
Text Label 2750 2050 2    50   ~ 0
a10
Text Label 2750 2150 2    50   ~ 0
a11
Text Label 2750 2250 2    50   ~ 0
a12
Text Label 2750 2350 2    50   ~ 0
a13
Text Label 2750 2450 2    50   ~ 0
a14
Text Label 4950 3450 0    50   ~ 0
a1
Text Label 4950 3550 0    50   ~ 0
a2
Text Label 4950 3650 0    50   ~ 0
a3
Text Label 4950 3750 0    50   ~ 0
a4
Text Label 4950 3850 0    50   ~ 0
a5
Text Label 4950 3950 0    50   ~ 0
a6
Text Label 4950 4050 0    50   ~ 0
a7
Text Label 4950 4150 0    50   ~ 0
a8
Text Label 4950 4250 0    50   ~ 0
a9
Text Label 4950 4350 0    50   ~ 0
a10
Text Label 4950 4450 0    50   ~ 0
a11
Text Label 4950 4550 0    50   ~ 0
a12
Text Label 4950 4650 0    50   ~ 0
a13
Text Label 4950 4750 0    50   ~ 0
a14
$Comp
L ddr:SK-hynix_H5TQ4G63CFR-RDC U?
U 1 1 5E62D438
P 9100 3550
AR Path="/5E62D438" Ref="U?"  Part="1" 
AR Path="/5E623205/5E62D438" Ref="U3"  Part="1" 
F 0 "U3" H 9100 6075 50  0000 C CNN
F 1 "SK-hynix_H5TQ4G63CFR-RDC" H 9100 5984 50  0000 C CNN
F 2 "package_bga:BGA-96_7.5x13.0mm_Layout2x3x16_P0.8mm_Ball0.4mm_Pad0.3mm" H 9500 4800 50  0001 C CNN
F 3 "" H 9500 4800 50  0001 C CNN
	1    9100 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 3450 8650 3450
Wire Wire Line
	8450 3550 8650 3550
Wire Wire Line
	8650 3650 8450 3650
Wire Wire Line
	8450 3750 8650 3750
Wire Wire Line
	8650 3850 8450 3850
Wire Wire Line
	8450 3950 8650 3950
Wire Wire Line
	8650 4050 8450 4050
Wire Wire Line
	8450 4150 8650 4150
Wire Wire Line
	8650 4250 8450 4250
Wire Wire Line
	8450 4350 8650 4350
Wire Wire Line
	8650 4450 8450 4450
Wire Wire Line
	8450 4550 8650 4550
Wire Wire Line
	8650 4650 8450 4650
Wire Wire Line
	8450 4750 8650 4750
Wire Wire Line
	8650 4850 8450 4850
Text Label 8450 3450 0    50   ~ 0
a0
Text Label 8450 3550 0    50   ~ 0
a1
Text Label 8450 3650 0    50   ~ 0
a2
Text Label 8450 3750 0    50   ~ 0
a3
Text Label 8450 3850 0    50   ~ 0
a4
Text Label 8450 3950 0    50   ~ 0
a5
Text Label 8450 4050 0    50   ~ 0
a6
Text Label 8450 4150 0    50   ~ 0
a7
Text Label 8450 4250 0    50   ~ 0
a8
Text Label 8450 4350 0    50   ~ 0
a9
Text Label 8450 4450 0    50   ~ 0
a10
Text Label 8450 4550 0    50   ~ 0
a11
Text Label 8450 4650 0    50   ~ 0
a12
Text Label 8450 4750 0    50   ~ 0
a13
Text Label 8450 4850 0    50   ~ 0
a14
Text Label 4950 3350 0    50   ~ 0
a0
$Comp
L cpu_ingenic:jz4780 U?
U 1 1 5E6233AF
P 1850 3050
AR Path="/5E6233AF" Ref="U?"  Part="1" 
AR Path="/5E623205/5E6233AF" Ref="U1"  Part="1" 
F 0 "U1" H 1850 5315 50  0000 C CNN
F 1 "jz4780" H 1850 5224 50  0000 C CNN
F 2 "ingenic:jz4780" H 900 4400 50  0001 C CNN
F 3 "ftp://ftp.ingenic.com/SOC/JZ4780/JZ4780_DS.PDF" H 2000 5350 50  0001 C CNN
	1    1850 3050
	1    0    0    -1  
$EndComp
Text Label 2750 1150 2    50   ~ 0
a1
Text Label 2750 1050 2    50   ~ 0
a0
Wire Wire Line
	2750 1050 2600 1050
Wire Wire Line
	2600 1150 2750 1150
Wire Wire Line
	2600 1250 2750 1250
Wire Wire Line
	2750 1350 2600 1350
Wire Wire Line
	2600 1450 2750 1450
Wire Wire Line
	2750 1550 2600 1550
Wire Wire Line
	2600 1650 2750 1650
Wire Wire Line
	2750 1750 2600 1750
Wire Wire Line
	2600 1850 2750 1850
Wire Wire Line
	2750 1950 2600 1950
Wire Wire Line
	2600 2050 2750 2050
Wire Wire Line
	2750 2150 2600 2150
Wire Wire Line
	2600 2250 2750 2250
Wire Wire Line
	2750 2350 2600 2350
Wire Wire Line
	2600 2450 2750 2450
Wire Wire Line
	4950 4750 5150 4750
Wire Wire Line
	5150 4650 4950 4650
Wire Wire Line
	4950 4550 5150 4550
Wire Wire Line
	5150 4450 4950 4450
Wire Wire Line
	4950 4350 5150 4350
Wire Wire Line
	5150 4250 4950 4250
Wire Wire Line
	4950 4150 5150 4150
Wire Wire Line
	5150 4050 4950 4050
Wire Wire Line
	4950 3950 5150 3950
Wire Wire Line
	5150 3850 4950 3850
Wire Wire Line
	4950 3750 5150 3750
Wire Wire Line
	5150 3650 4950 3650
Wire Wire Line
	4950 3550 5150 3550
Wire Wire Line
	5150 3450 4950 3450
Wire Wire Line
	4950 3350 5150 3350
Wire Wire Line
	950  4650 1100 4650
Wire Wire Line
	1100 4750 950  4750
Wire Wire Line
	950  4850 1100 4850
Text Label 950  4650 0    50   ~ 0
ba0
Text Label 950  4750 0    50   ~ 0
ba1
Text Label 950  4850 0    50   ~ 0
ba2
Wire Wire Line
	4950 4850 5150 4850
Wire Wire Line
	5150 4950 4950 4950
Wire Wire Line
	4950 5050 5150 5050
Text Label 4950 4850 0    50   ~ 0
ba0
Text Label 4950 4950 0    50   ~ 0
ba1
Text Label 4950 5050 0    50   ~ 0
ba2
Wire Wire Line
	8450 4950 8650 4950
Wire Wire Line
	8650 5050 8450 5050
Wire Wire Line
	8450 5150 8650 5150
Text Label 8450 4950 0    50   ~ 0
ba0
Text Label 8450 5050 0    50   ~ 0
ba1
Text Label 8450 5150 0    50   ~ 0
ba2
Wire Wire Line
	2600 4350 2850 4350
Text Label 2850 4350 2    50   ~ 0
odt
Wire Wire Line
	6050 4450 6250 4450
Text Label 6250 4450 2    50   ~ 0
odt
Wire Wire Line
	9550 4550 9750 4550
Text Label 9750 4550 2    50   ~ 0
odt
Wire Wire Line
	2600 4250 2850 4250
Text Label 2850 4250 2    50   ~ 0
cke
Wire Wire Line
	6050 4550 6250 4550
Text Label 6250 4550 2    50   ~ 0
cke
Wire Wire Line
	9550 4650 9750 4650
Text Label 9750 4650 2    50   ~ 0
cke
NoConn ~ 2600 2850
Wire Wire Line
	2600 2750 2850 2750
Text Label 2850 2750 2    50   ~ 0
cs0_n
Wire Wire Line
	6050 4750 6350 4750
Text Label 6350 4750 2    50   ~ 0
cs0_n
Wire Wire Line
	9550 4850 9850 4850
Text Label 9850 4850 2    50   ~ 0
cs0_n
Wire Wire Line
	2600 2950 2850 2950
Text Label 2850 2950 2    50   ~ 0
ras_n
Wire Wire Line
	6050 4950 6350 4950
Text Label 6350 4950 2    50   ~ 0
ras_n
Wire Wire Line
	9550 5050 9850 5050
Text Label 9850 5050 2    50   ~ 0
ras_n
Wire Wire Line
	2600 3050 2850 3050
Text Label 2850 3050 2    50   ~ 0
cas_n
Wire Wire Line
	6050 4850 6350 4850
Text Label 6350 4850 2    50   ~ 0
cas_n
Wire Wire Line
	9550 4950 9850 4950
Text Label 9850 4950 2    50   ~ 0
cas_n
Wire Wire Line
	9550 5150 9850 5150
Text Label 9850 5150 2    50   ~ 0
we_n
Wire Wire Line
	6050 5050 6350 5050
Text Label 6350 5050 2    50   ~ 0
we_n
Wire Wire Line
	2600 3150 2850 3150
Text Label 2850 3150 2    50   ~ 0
we_n
Text Label 4850 3300 2    50   ~ 0
ck+
Text Label 4850 3550 2    50   ~ 0
ck-
Text Label 4950 5650 0    50   ~ 0
ck-
Text Label 4950 5550 0    50   ~ 0
ck+
Text Label 8450 5650 0    50   ~ 0
ck+
Wire Wire Line
	6050 4650 6500 4650
Text Label 6500 4650 2    50   ~ 0
mem_reset_n
Wire Wire Line
	9550 4750 10000 4750
Text Label 10000 4750 2    50   ~ 0
mem_reset_n
Wire Wire Line
	2600 4450 3300 4450
Text Label 3300 4450 2    50   ~ 0
mem_reset_n
NoConn ~ 9550 5550
NoConn ~ 9550 5650
NoConn ~ 9550 5750
NoConn ~ 9550 5850
NoConn ~ 9550 5950
NoConn ~ 6050 5850
NoConn ~ 6050 5750
NoConn ~ 6050 5650
NoConn ~ 6050 5550
NoConn ~ 6050 5450
Wire Wire Line
	1100 1050 900  1050
Wire Wire Line
	900  1150 1100 1150
Wire Wire Line
	1100 1250 900  1250
Wire Wire Line
	900  1350 1100 1350
Wire Wire Line
	1100 1450 900  1450
Wire Wire Line
	900  1550 1100 1550
Wire Wire Line
	1100 1650 900  1650
Wire Wire Line
	900  1750 1100 1750
Wire Wire Line
	1100 1850 900  1850
Wire Wire Line
	900  1950 1100 1950
Wire Wire Line
	1100 2050 900  2050
Wire Wire Line
	900  2150 1100 2150
Wire Wire Line
	1100 2250 900  2250
Wire Wire Line
	900  2350 1100 2350
Wire Wire Line
	1100 2450 900  2450
Wire Wire Line
	900  2550 1100 2550
Wire Wire Line
	1100 2650 900  2650
Wire Wire Line
	900  2750 1100 2750
Wire Wire Line
	1100 2850 900  2850
Wire Wire Line
	900  2950 1100 2950
Wire Wire Line
	1100 3050 900  3050
Wire Wire Line
	900  3150 1100 3150
Wire Wire Line
	1100 3250 900  3250
Wire Wire Line
	900  3350 1100 3350
Wire Wire Line
	1100 3450 900  3450
Wire Wire Line
	900  3550 1100 3550
Wire Wire Line
	1100 3650 900  3650
Wire Wire Line
	900  3750 1100 3750
Wire Wire Line
	1100 3850 900  3850
Wire Wire Line
	900  3950 1100 3950
Wire Wire Line
	1100 4050 900  4050
Wire Wire Line
	900  4150 1100 4150
Wire Wire Line
	1100 4250 900  4250
Wire Wire Line
	900  4350 1100 4350
Wire Wire Line
	1100 4450 900  4450
Wire Wire Line
	900  4550 1100 4550
Text Label 900  4550 0    50   ~ 0
dm3
Text Label 900  4450 0    50   ~ 0
dm2
Text Label 900  4350 0    50   ~ 0
dm1
Text Label 900  4250 0    50   ~ 0
dm0
Text Label 900  1050 0    50   ~ 0
dq0
Text Label 900  1150 0    50   ~ 0
dq1
Text Label 900  1250 0    50   ~ 0
dq2
Text Label 900  1350 0    50   ~ 0
dq3
Text Label 900  1450 0    50   ~ 0
dq4
Text Label 900  1550 0    50   ~ 0
dq5
Text Label 900  1650 0    50   ~ 0
dq6
Text Label 900  1750 0    50   ~ 0
dq7
Text Label 900  1850 0    50   ~ 0
dq8
Text Label 900  1950 0    50   ~ 0
dq9
Text Label 900  2050 0    50   ~ 0
dq10
Text Label 900  2150 0    50   ~ 0
dq11
Text Label 900  2250 0    50   ~ 0
dq12
Text Label 900  2350 0    50   ~ 0
dq13
Text Label 900  2450 0    50   ~ 0
dq14
Text Label 900  2550 0    50   ~ 0
dq15
Text Label 900  2650 0    50   ~ 0
dq16
Text Label 900  2750 0    50   ~ 0
dq17
Text Label 900  2850 0    50   ~ 0
dq18
Text Label 900  2950 0    50   ~ 0
dq19
Text Label 900  3050 0    50   ~ 0
dq20
Text Label 900  3150 0    50   ~ 0
dq21
Text Label 900  3250 0    50   ~ 0
dq22
Text Label 900  3350 0    50   ~ 0
dq23
Text Label 900  3450 0    50   ~ 0
dq24
Text Label 900  3550 0    50   ~ 0
dq25
Text Label 900  3650 0    50   ~ 0
dq26
Text Label 900  3750 0    50   ~ 0
dq27
Text Label 900  3850 0    50   ~ 0
dq28
Text Label 900  3950 0    50   ~ 0
dq29
Text Label 900  4050 0    50   ~ 0
dq30
Text Label 900  4150 0    50   ~ 0
dq31
Wire Wire Line
	5150 3250 4950 3250
Wire Wire Line
	4950 3250 4950 3150
$Comp
L power:GND #PWR04
U 1 1 5E707D8E
P 4950 800
F 0 "#PWR04" H 4950 550 50  0001 C CNN
F 1 "GND" H 4955 627 50  0000 C CNN
F 2 "" H 4950 800 50  0001 C CNN
F 3 "" H 4950 800 50  0001 C CNN
	1    4950 800 
	-1   0    0    1   
$EndComp
Wire Wire Line
	5150 1150 4950 1150
Connection ~ 4950 1150
Wire Wire Line
	4950 1150 4950 800 
Wire Wire Line
	5150 1250 4950 1250
Connection ~ 4950 1250
Wire Wire Line
	4950 1250 4950 1150
Wire Wire Line
	5150 1350 4950 1350
Connection ~ 4950 1350
Wire Wire Line
	4950 1350 4950 1250
Wire Wire Line
	5150 1450 4950 1450
Connection ~ 4950 1450
Wire Wire Line
	4950 1450 4950 1350
Wire Wire Line
	5150 1550 4950 1550
Connection ~ 4950 1550
Wire Wire Line
	4950 1550 4950 1450
Wire Wire Line
	5150 1650 4950 1650
Connection ~ 4950 1650
Wire Wire Line
	4950 1650 4950 1550
Wire Wire Line
	5150 1750 4950 1750
Connection ~ 4950 1750
Wire Wire Line
	4950 1750 4950 1650
Wire Wire Line
	5150 1850 4950 1850
Connection ~ 4950 1850
Wire Wire Line
	4950 1850 4950 1750
Wire Wire Line
	5150 1950 4950 1950
Connection ~ 4950 1950
Wire Wire Line
	4950 1950 4950 1850
Wire Wire Line
	5150 2050 4950 2050
Connection ~ 4950 2050
Wire Wire Line
	4950 2050 4950 1950
Wire Wire Line
	5150 2150 4950 2150
Connection ~ 4950 2150
Wire Wire Line
	4950 2150 4950 2050
Wire Wire Line
	5150 2250 4950 2250
Connection ~ 4950 2250
Wire Wire Line
	4950 2250 4950 2150
Wire Wire Line
	5150 2350 4950 2350
Connection ~ 4950 2350
Wire Wire Line
	4950 2350 4950 2250
Wire Wire Line
	5150 2450 4950 2450
Connection ~ 4950 2450
Wire Wire Line
	4950 2450 4950 2350
Wire Wire Line
	5150 2550 4950 2550
Connection ~ 4950 2550
Wire Wire Line
	4950 2550 4950 2450
Wire Wire Line
	5150 2650 4950 2650
Connection ~ 4950 2650
Wire Wire Line
	4950 2650 4950 2550
Wire Wire Line
	5150 2750 4950 2750
Connection ~ 4950 2750
Wire Wire Line
	4950 2750 4950 2650
Wire Wire Line
	5150 2850 4950 2850
Connection ~ 4950 2850
Wire Wire Line
	4950 2850 4950 2750
Wire Wire Line
	5150 2950 4950 2950
Connection ~ 4950 2950
Wire Wire Line
	4950 2950 4950 2850
Wire Wire Line
	5150 3050 4950 3050
Connection ~ 4950 3050
Wire Wire Line
	4950 3050 4950 2950
Wire Wire Line
	5150 3150 4950 3150
Connection ~ 4950 3150
Wire Wire Line
	4950 3150 4950 3050
Wire Wire Line
	8650 3350 8450 3350
Wire Wire Line
	8450 3350 8450 3250
$Comp
L power:GND #PWR09
U 1 1 5E7E0543
P 8450 850
F 0 "#PWR09" H 8450 600 50  0001 C CNN
F 1 "GND" H 8455 677 50  0000 C CNN
F 2 "" H 8450 850 50  0001 C CNN
F 3 "" H 8450 850 50  0001 C CNN
	1    8450 850 
	-1   0    0    1   
$EndComp
Wire Wire Line
	8650 3250 8450 3250
Connection ~ 8450 3250
Wire Wire Line
	8450 3250 8450 3150
Wire Wire Line
	8650 3150 8450 3150
Connection ~ 8450 3150
Wire Wire Line
	8450 3150 8450 3050
Wire Wire Line
	8650 3050 8450 3050
Connection ~ 8450 3050
Wire Wire Line
	8450 3050 8450 2950
Wire Wire Line
	8650 2950 8450 2950
Connection ~ 8450 2950
Wire Wire Line
	8450 2950 8450 2850
Wire Wire Line
	8650 2850 8450 2850
Connection ~ 8450 2850
Wire Wire Line
	8450 2850 8450 2750
Wire Wire Line
	8650 2750 8450 2750
Connection ~ 8450 2750
Wire Wire Line
	8450 2750 8450 2650
Wire Wire Line
	8650 2650 8450 2650
Connection ~ 8450 2650
Wire Wire Line
	8450 2650 8450 2550
Wire Wire Line
	8650 2550 8450 2550
Connection ~ 8450 2550
Wire Wire Line
	8450 2550 8450 2450
Wire Wire Line
	8650 2450 8450 2450
Connection ~ 8450 2450
Wire Wire Line
	8450 2450 8450 2350
Wire Wire Line
	8650 2350 8450 2350
Connection ~ 8450 2350
Wire Wire Line
	8450 2350 8450 2250
Wire Wire Line
	8650 2250 8450 2250
Connection ~ 8450 2250
Wire Wire Line
	8450 2250 8450 2150
Wire Wire Line
	8650 2150 8450 2150
Connection ~ 8450 2150
Wire Wire Line
	8450 2150 8450 2050
Wire Wire Line
	8650 2050 8450 2050
Connection ~ 8450 2050
Wire Wire Line
	8450 2050 8450 1950
Wire Wire Line
	8650 1950 8450 1950
Connection ~ 8450 1950
Wire Wire Line
	8450 1950 8450 1850
Wire Wire Line
	8650 1850 8450 1850
Connection ~ 8450 1850
Wire Wire Line
	8450 1850 8450 1750
Wire Wire Line
	8650 1750 8450 1750
Connection ~ 8450 1750
Wire Wire Line
	8450 1750 8450 1650
Wire Wire Line
	8650 1650 8450 1650
Connection ~ 8450 1650
Wire Wire Line
	8450 1650 8450 1550
Wire Wire Line
	8650 1550 8450 1550
Connection ~ 8450 1550
Wire Wire Line
	8450 1550 8450 1450
Wire Wire Line
	8650 1450 8450 1450
Connection ~ 8450 1450
Wire Wire Line
	8450 1450 8450 1350
Wire Wire Line
	8650 1350 8450 1350
Connection ~ 8450 1350
Wire Wire Line
	8450 1350 8450 1250
Wire Wire Line
	8650 1250 8450 1250
Connection ~ 8450 1250
Wire Wire Line
	8450 1250 8450 850 
Wire Wire Line
	5150 5850 4950 5850
Text Label 4950 5850 0    50   ~ 0
dm0
Wire Wire Line
	5150 5750 4950 5750
Text Label 4950 5750 0    50   ~ 0
dm1
Wire Wire Line
	8650 5950 8450 5950
Text Label 8450 5950 0    50   ~ 0
dm2
Wire Wire Line
	8650 5850 8450 5850
Text Label 8450 5850 0    50   ~ 0
dm3
Wire Wire Line
	6050 2850 6300 2850
Wire Wire Line
	6300 2950 6050 2950
Wire Wire Line
	6050 3050 6300 3050
Wire Wire Line
	6300 3150 6050 3150
Wire Wire Line
	6050 3250 6300 3250
Wire Wire Line
	6300 3350 6050 3350
Wire Wire Line
	6050 3450 6300 3450
Wire Wire Line
	6300 3550 6050 3550
Wire Wire Line
	6050 3650 6300 3650
Wire Wire Line
	6300 3750 6050 3750
Wire Wire Line
	6050 3850 6300 3850
Wire Wire Line
	6300 3950 6050 3950
Wire Wire Line
	6050 4050 6300 4050
Wire Wire Line
	6300 4150 6050 4150
Wire Wire Line
	6050 4250 6300 4250
Wire Wire Line
	6300 4350 6050 4350
Text Label 6300 2850 2    50   ~ 0
dq0
Text Label 6300 2950 2    50   ~ 0
dq1
Text Label 6300 3050 2    50   ~ 0
dq2
Text Label 6300 3150 2    50   ~ 0
dq3
Text Label 6300 3250 2    50   ~ 0
dq4
Text Label 6300 3350 2    50   ~ 0
dq5
Text Label 6300 3450 2    50   ~ 0
dq6
Text Label 6300 3550 2    50   ~ 0
dq7
Text Label 6300 3650 2    50   ~ 0
dq8
Text Label 6300 3750 2    50   ~ 0
dq9
Text Label 6300 3850 2    50   ~ 0
dq10
Text Label 6300 3950 2    50   ~ 0
dq11
Text Label 6300 4050 2    50   ~ 0
dq12
Text Label 6300 4150 2    50   ~ 0
dq13
Text Label 6300 4250 2    50   ~ 0
dq14
Text Label 6300 4350 2    50   ~ 0
dq15
Wire Wire Line
	9550 2950 9800 2950
Wire Wire Line
	9800 3050 9550 3050
Wire Wire Line
	9550 3150 9800 3150
Wire Wire Line
	9800 3250 9550 3250
Wire Wire Line
	9550 3350 9800 3350
Wire Wire Line
	9800 3450 9550 3450
Wire Wire Line
	9550 3550 9800 3550
Wire Wire Line
	9800 3650 9550 3650
Wire Wire Line
	9550 3750 9800 3750
Wire Wire Line
	9800 3850 9550 3850
Wire Wire Line
	9550 3950 9800 3950
Wire Wire Line
	9800 4050 9550 4050
Wire Wire Line
	9550 4150 9800 4150
Wire Wire Line
	9800 4250 9550 4250
Wire Wire Line
	9550 4350 9800 4350
Wire Wire Line
	9800 4450 9550 4450
Text Label 9800 3450 2    50   ~ 0
dq16
Text Label 9800 3650 2    50   ~ 0
dq17
Text Label 9800 3050 2    50   ~ 0
dq18
Text Label 9800 3250 2    50   ~ 0
dq19
Text Label 9800 2950 2    50   ~ 0
dq20
Text Label 9800 3350 2    50   ~ 0
dq21
Text Label 9800 3550 2    50   ~ 0
dq22
Text Label 9800 3150 2    50   ~ 0
dq23
Text Label 9800 3750 2    50   ~ 0
dq24
Text Label 9800 4350 2    50   ~ 0
dq25
Text Label 9800 3950 2    50   ~ 0
dq26
Text Label 9800 4150 2    50   ~ 0
dq27
Text Label 9800 3850 2    50   ~ 0
dq28
Text Label 9800 4050 2    50   ~ 0
dq29
Text Label 9800 4450 2    50   ~ 0
dq30
Text Label 9800 4250 2    50   ~ 0
dq31
Wire Wire Line
	2600 3250 2950 3250
Text Label 4800 1700 2    50   ~ 0
dqs0+
Text Label 4800 1800 2    50   ~ 0
dqs0-
Text Label 4800 1950 2    50   ~ 0
dqs1+
Text Label 4800 2050 2    50   ~ 0
dqs1-
Text Label 4800 2200 2    50   ~ 0
dqs2+
Text Label 4800 2300 2    50   ~ 0
dqs2-
Text Label 4800 2450 2    50   ~ 0
dqs3+
Text Label 4800 2550 2    50   ~ 0
dqs3-
Wire Wire Line
	5150 5350 4850 5350
Wire Wire Line
	5150 5450 4850 5450
Wire Wire Line
	4850 5250 5150 5250
Wire Wire Line
	5150 5150 4850 5150
Text Label 4850 5350 0    50   ~ 0
dqs0+
Text Label 4850 5450 0    50   ~ 0
dqs0-
Text Label 4850 5150 0    50   ~ 0
dqs1+
Text Label 4850 5250 0    50   ~ 0
dqs1-
Wire Wire Line
	8650 5550 8350 5550
Wire Wire Line
	8350 5450 8650 5450
Wire Wire Line
	8650 5350 8350 5350
Wire Wire Line
	8350 5250 8650 5250
Text Label 8350 5450 0    50   ~ 0
dqs2+
Text Label 8350 5550 0    50   ~ 0
dqs2-
Text Label 8350 5250 0    50   ~ 0
dqs3+
Text Label 8350 5350 0    50   ~ 0
dqs3-
Wire Wire Line
	6050 2750 6300 2750
Wire Wire Line
	6300 2750 6300 2650
Wire Wire Line
	6050 2650 6300 2650
Connection ~ 6300 2650
Wire Wire Line
	6300 2650 6300 2550
Wire Wire Line
	6050 2550 6300 2550
Connection ~ 6300 2550
Wire Wire Line
	6300 2550 6300 2450
Wire Wire Line
	6050 2450 6300 2450
Connection ~ 6300 2450
Wire Wire Line
	6300 2450 6300 2350
Wire Wire Line
	6050 2350 6300 2350
Connection ~ 6300 2350
Wire Wire Line
	6300 2350 6300 2250
Wire Wire Line
	6050 2250 6300 2250
Connection ~ 6300 2250
Wire Wire Line
	6300 2250 6300 2150
Wire Wire Line
	6050 2150 6300 2150
Connection ~ 6300 2150
Wire Wire Line
	6300 2150 6300 2050
Wire Wire Line
	6050 2050 6300 2050
Connection ~ 6300 2050
Wire Wire Line
	6300 2050 6300 1950
Wire Wire Line
	6050 1950 6300 1950
Connection ~ 6300 1950
Wire Wire Line
	6300 1950 6300 1850
Wire Wire Line
	6050 1850 6300 1850
Connection ~ 6300 1850
Wire Wire Line
	6300 1850 6300 1750
Wire Wire Line
	6050 1750 6300 1750
Connection ~ 6300 1750
Wire Wire Line
	6300 1750 6300 1650
Wire Wire Line
	6050 1650 6300 1650
Connection ~ 6300 1650
Wire Wire Line
	6300 1650 6300 1550
Wire Wire Line
	6050 1550 6300 1550
Connection ~ 6300 1550
Wire Wire Line
	6300 1550 6300 1450
Wire Wire Line
	6050 1450 6300 1450
Connection ~ 6300 1450
Wire Wire Line
	6300 1450 6300 1350
Wire Wire Line
	6050 1350 6300 1350
Connection ~ 6300 1350
Wire Wire Line
	6300 1350 6300 1250
Wire Wire Line
	6050 1250 6300 1250
Connection ~ 6300 1250
Wire Wire Line
	6300 1250 6300 1150
Wire Wire Line
	6050 1150 6300 1150
Connection ~ 6300 1150
Wire Wire Line
	6300 1150 6300 1000
Wire Wire Line
	9550 2850 9800 2850
Wire Wire Line
	9800 2850 9800 2750
Wire Wire Line
	9550 2750 9800 2750
Connection ~ 9800 2750
Wire Wire Line
	9800 2750 9800 2650
Wire Wire Line
	9550 2650 9800 2650
Connection ~ 9800 2650
Wire Wire Line
	9800 2650 9800 2550
Wire Wire Line
	9550 2550 9800 2550
Connection ~ 9800 2550
Wire Wire Line
	9800 2550 9800 2450
Wire Wire Line
	9550 2450 9800 2450
Connection ~ 9800 2450
Wire Wire Line
	9800 2450 9800 2350
Wire Wire Line
	9550 2350 9800 2350
Connection ~ 9800 2350
Wire Wire Line
	9800 2350 9800 2250
Wire Wire Line
	9550 2250 9800 2250
Connection ~ 9800 2250
Wire Wire Line
	9800 2250 9800 2150
Wire Wire Line
	9550 2150 9800 2150
Connection ~ 9800 2150
Wire Wire Line
	9800 2150 9800 2050
Wire Wire Line
	9550 2050 9800 2050
Connection ~ 9800 2050
Wire Wire Line
	9800 2050 9800 1950
Wire Wire Line
	9550 1950 9800 1950
Connection ~ 9800 1950
Wire Wire Line
	9800 1950 9800 1850
Wire Wire Line
	9550 1850 9800 1850
Connection ~ 9800 1850
Wire Wire Line
	9800 1850 9800 1750
Wire Wire Line
	9550 1750 9800 1750
Connection ~ 9800 1750
Wire Wire Line
	9800 1750 9800 1650
Wire Wire Line
	9550 1650 9800 1650
Connection ~ 9800 1650
Wire Wire Line
	9800 1650 9800 1550
Wire Wire Line
	9550 1550 9800 1550
Connection ~ 9800 1550
Wire Wire Line
	9800 1550 9800 1450
Wire Wire Line
	9550 1450 9800 1450
Connection ~ 9800 1450
Wire Wire Line
	9800 1450 9800 1350
Wire Wire Line
	9550 1350 9800 1350
Connection ~ 9800 1350
Wire Wire Line
	9800 1350 9800 1250
Wire Wire Line
	9550 1250 9800 1250
Connection ~ 9800 1250
Wire Wire Line
	9800 1250 9800 1100
Wire Wire Line
	9550 5350 9850 5350
Wire Wire Line
	9850 5350 9850 5250
Wire Wire Line
	9850 5250 9550 5250
Wire Wire Line
	6050 5250 6350 5250
Wire Wire Line
	6350 5250 6350 5150
Wire Wire Line
	6350 5150 6050 5150
Wire Wire Line
	6050 5350 6250 5350
Wire Wire Line
	6250 5350 6250 5450
Wire Wire Line
	9550 5450 9750 5450
Wire Wire Line
	9750 5450 9750 5550
$Comp
L power:GND #PWR05
U 1 1 5EF3E79D
P 6250 5800
F 0 "#PWR05" H 6250 5550 50  0001 C CNN
F 1 "GND" H 6255 5627 50  0000 C CNN
F 2 "" H 6250 5800 50  0001 C CNN
F 3 "" H 6250 5800 50  0001 C CNN
	1    6250 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 5800 6250 5750
$Comp
L power:GND #PWR010
U 1 1 5EF61EA0
P 9750 5900
F 0 "#PWR010" H 9750 5650 50  0001 C CNN
F 1 "GND" H 9755 5727 50  0000 C CNN
F 2 "" H 9750 5900 50  0001 C CNN
F 3 "" H 9750 5900 50  0001 C CNN
	1    9750 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 5900 9750 5850
$Comp
L Device:C C18
U 1 1 5EFF0B9E
P 6650 5400
F 0 "C18" V 6700 5450 50  0000 L CNN
F 1 "100nF" V 6700 5100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6688 5250 50  0001 C CNN
F 3 "~" H 6650 5400 50  0001 C CNN
	1    6650 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C17
U 1 1 5EFF0BD2
P 6650 4750
F 0 "C17" V 6700 4800 50  0000 L CNN
F 1 "100nF" V 6700 4450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6688 4600 50  0001 C CNN
F 3 "~" H 6650 4750 50  0001 C CNN
	1    6650 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5EFF0C0A
P 7050 5800
F 0 "#PWR07" H 7050 5550 50  0001 C CNN
F 1 "GND" H 7055 5627 50  0000 C CNN
F 2 "" H 7050 5800 50  0001 C CNN
F 3 "" H 7050 5800 50  0001 C CNN
	1    7050 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 5550 6650 5800
Wire Wire Line
	7050 5350 7050 5150
Wire Wire Line
	6650 4900 6650 5150
Wire Wire Line
	6350 5150 6650 5150
Connection ~ 6650 5150
Wire Wire Line
	6650 5150 6650 5250
Wire Wire Line
	6650 5150 7050 5150
Connection ~ 7050 5150
Wire Wire Line
	7050 5150 7050 5000
Wire Wire Line
	6650 4600 6650 4550
Wire Wire Line
	6650 4550 7050 4550
Wire Wire Line
	7050 4550 7050 4700
Connection ~ 6350 5150
$Comp
L Device:C C40
U 1 1 5F13E409
P 10150 5500
F 0 "C40" V 10200 5550 50  0000 L CNN
F 1 "100nF" V 10200 5200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10188 5350 50  0001 C CNN
F 3 "~" H 10150 5500 50  0001 C CNN
	1    10150 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C39
U 1 1 5F13E410
P 10150 4850
F 0 "C39" V 10200 4900 50  0000 L CNN
F 1 "100nF" V 10200 4550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10188 4700 50  0001 C CNN
F 3 "~" H 10150 4850 50  0001 C CNN
	1    10150 4850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5F13E417
P 10550 5900
F 0 "#PWR012" H 10550 5650 50  0001 C CNN
F 1 "GND" H 10555 5727 50  0000 C CNN
F 2 "" H 10550 5900 50  0001 C CNN
F 3 "" H 10550 5900 50  0001 C CNN
	1    10550 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 5650 10150 5900
Wire Wire Line
	10550 5900 10550 5750
Wire Wire Line
	10550 5450 10550 5250
Wire Wire Line
	10150 5000 10150 5250
Wire Wire Line
	9850 5250 10150 5250
Connection ~ 10150 5250
Wire Wire Line
	10150 5250 10150 5350
Wire Wire Line
	10150 5250 10550 5250
Connection ~ 10550 5250
Wire Wire Line
	10550 5250 10550 5100
Wire Wire Line
	10150 4700 10150 4650
Wire Wire Line
	10150 4650 10550 4650
Wire Wire Line
	10550 4650 10550 4800
Connection ~ 9850 5250
$Comp
L Device:R R1
U 1 1 5F18CA58
P 2650 5100
F 0 "R1" V 2550 5050 50  0000 L CNN
F 1 "240" V 2650 5000 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" V 2580 5100 50  0001 C CNN
F 3 "~" H 2650 5100 50  0001 C CNN
F 4 "1%" V 2750 5050 50  0000 C CNN "tolerance"
	1    2650 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 4850 2650 4850
Wire Wire Line
	2650 4850 2650 4950
$Comp
L power:GND #PWR01
U 1 1 5F18CA61
P 2650 5300
F 0 "#PWR01" H 2650 5050 50  0001 C CNN
F 1 "GND" H 2655 5127 50  0000 C CNN
F 2 "" H 2650 5300 50  0001 C CNN
F 3 "" H 2650 5300 50  0001 C CNN
	1    2650 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 5300 2650 5250
$Comp
L Device:R R13
U 1 1 5F1B4626
P 4100 4900
F 0 "R13" V 4000 4850 50  0000 L CNN
F 1 "2k" V 4100 4850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" V 4030 4900 50  0001 C CNN
F 3 "~" H 4100 4900 50  0001 C CNN
F 4 "1%" V 4200 4900 50  0000 C CNN "tolerance"
	1    4100 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5F1B4634
P 3700 4950
F 0 "C4" V 3750 5000 50  0000 L CNN
F 1 "100nF" V 3750 4650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3738 4800 50  0001 C CNN
F 3 "~" H 3700 4950 50  0001 C CNN
	1    3700 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5F1B463B
P 3700 4150
F 0 "C3" V 3750 4200 50  0000 L CNN
F 1 "100nF" V 3750 3850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3738 4000 50  0001 C CNN
F 3 "~" H 3700 4150 50  0001 C CNN
	1    3700 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5F1B4642
P 3900 5300
F 0 "#PWR03" H 3900 5050 50  0001 C CNN
F 1 "GND" H 3905 5127 50  0000 C CNN
F 2 "" H 3900 5300 50  0001 C CNN
F 3 "" H 3900 5300 50  0001 C CNN
	1    3900 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 5100 3700 5300
Wire Wire Line
	3700 5300 3900 5300
Wire Wire Line
	4100 5300 4100 5050
Wire Wire Line
	4100 4750 4100 4550
Wire Wire Line
	3700 4300 3700 4550
Wire Wire Line
	2600 4550 2900 4550
Connection ~ 3700 4550
Wire Wire Line
	3700 4550 3700 4800
Wire Wire Line
	3700 4550 4100 4550
Connection ~ 4100 4550
Wire Wire Line
	4100 4550 4100 4400
Wire Wire Line
	3700 4000 3700 3950
Wire Wire Line
	3700 3950 4100 3950
Wire Wire Line
	4100 3950 4100 4100
Wire Wire Line
	3900 5300 4100 5300
Wire Wire Line
	2600 4650 2900 4650
Wire Wire Line
	2900 4650 2900 4550
Wire Wire Line
	2900 4550 3400 4550
Wire Wire Line
	2600 4750 2900 4750
Wire Wire Line
	2900 4750 2900 4650
Connection ~ 2900 4650
$Comp
L Device:C C1
U 1 1 5F33C49A
P 3150 4800
F 0 "C1" V 3200 4850 50  0000 L CNN
F 1 "100nF" V 3200 4500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3188 4650 50  0001 C CNN
F 3 "~" H 3150 4800 50  0001 C CNN
	1    3150 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 4650 3150 4650
Connection ~ 3900 5300
Connection ~ 3700 5300
Wire Wire Line
	3150 4950 3150 5300
Wire Wire Line
	3150 5300 3400 5300
$Comp
L Device:C C7
U 1 1 5F522E2C
P 6450 5400
F 0 "C7" V 6500 5450 50  0000 L CNN
F 1 "100nF" V 6500 5100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6488 5250 50  0001 C CNN
F 3 "~" H 6450 5400 50  0001 C CNN
	1    6450 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 5250 6450 5250
Connection ~ 6350 5250
Wire Wire Line
	6450 5550 6450 5800
Wire Wire Line
	6450 5800 6650 5800
Connection ~ 6650 5800
$Comp
L Device:C C29
U 1 1 5F57F561
P 9950 5500
F 0 "C29" V 10000 5550 50  0000 L CNN
F 1 "100nF" V 10000 5200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9988 5350 50  0001 C CNN
F 3 "~" H 9950 5500 50  0001 C CNN
	1    9950 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 5350 9850 5350
Connection ~ 9850 5350
Wire Wire Line
	9950 5650 9950 5900
Wire Wire Line
	9950 5900 10150 5900
Connection ~ 10150 5900
$Comp
L Device:C C6
U 1 1 5F5DCE6D
P 4350 4550
F 0 "C6" H 4400 4650 50  0000 L CNN
F 1 "10uF" H 4400 4450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4388 4400 50  0001 C CNN
F 3 "~" H 4350 4550 50  0001 C CNN
	1    4350 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 4700 4350 5300
Wire Wire Line
	4350 5300 4100 5300
Connection ~ 4100 5300
Wire Wire Line
	4350 4400 4350 3950
Wire Wire Line
	4350 3950 4100 3950
Connection ~ 4100 3950
$Comp
L Device:C C28
U 1 1 5F63BCEE
P 7300 5150
F 0 "C28" H 7350 5250 50  0000 L CNN
F 1 "10uF" H 7350 5050 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7338 5000 50  0001 C CNN
F 3 "~" H 7300 5150 50  0001 C CNN
	1    7300 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 5000 7300 4550
Wire Wire Line
	7300 4550 7050 4550
Connection ~ 7050 4550
Wire Wire Line
	7300 5300 7300 5800
Wire Wire Line
	7300 5800 7050 5800
$Comp
L Device:C C50
U 1 1 5F69C72E
P 10800 5250
F 0 "C50" H 10850 5350 50  0000 L CNN
F 1 "10uF" H 10850 5150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10838 5100 50  0001 C CNN
F 3 "~" H 10800 5250 50  0001 C CNN
	1    10800 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	10800 5100 10800 4650
Wire Wire Line
	10800 4650 10550 4650
Connection ~ 10550 4650
Wire Wire Line
	10800 5400 10800 5900
Wire Wire Line
	10800 5900 10550 5900
Connection ~ 10550 5900
$Comp
L Device:R R7
U 1 1 5E65E8C0
P 3650 3650
F 0 "R7" V 3550 3550 50  0000 C CNN
F 1 "8.2/10" V 3650 3700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" V 3580 3650 50  0001 C CNN
F 3 "~" H 3650 3650 50  0001 C CNN
F 4 "1%" V 3550 3750 50  0000 C CNN "tolerance"
	1    3650 3650
	0    1    1    0   
$EndComp
$Comp
L Device:R R14
U 1 1 5E65E926
P 4450 3400
F 0 "R14" V 4550 3500 50  0000 R CNN
F 1 "100" V 4450 3500 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" V 4380 3400 50  0001 C CNN
F 3 "~" H 4450 3400 50  0001 C CNN
F 4 "1%" V 4350 3400 50  0000 C CNN "tolerance"
	1    4450 3400
	-1   0    0    1   
$EndComp
Wire Wire Line
	4450 3150 4450 3250
Wire Wire Line
	4450 3650 4450 3550
Wire Wire Line
	3450 4150 3450 3650
Wire Wire Line
	3450 3650 3500 3650
Wire Wire Line
	2600 4150 3450 4150
Wire Wire Line
	3400 4050 3400 3150
Wire Wire Line
	3400 3150 3500 3150
Wire Wire Line
	2600 4050 3400 4050
Wire Notes Line
	3350 2950 4250 2950
Wire Notes Line
	4250 2950 4250 3750
Wire Notes Line
	4250 3750 3350 3750
Wire Notes Line
	3350 3750 3350 2950
Text Notes 3400 2900 0    50   ~ 0
place close to CPU
Wire Wire Line
	4950 5550 5150 5550
Wire Wire Line
	4950 5650 5150 5650
Wire Wire Line
	8450 5650 8650 5650
Wire Wire Line
	8450 5750 8650 5750
Text Label 8450 5750 0    50   ~ 0
ck-
Wire Notes Line
	4300 2950 4300 3750
Wire Notes Line
	4300 3750 4600 3750
Wire Notes Line
	4600 3750 4600 2950
Wire Notes Line
	4600 2950 4300 2950
Text Notes 4300 2900 0    50   ~ 0
place at\nbranching
$Comp
L Device:R R12
U 1 1 5EBC41DE
P 4100 4250
F 0 "R12" V 4000 4200 50  0000 L CNN
F 1 "2k" V 4100 4200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" V 4030 4250 50  0001 C CNN
F 3 "~" H 4100 4250 50  0001 C CNN
F 4 "1%" V 4200 4250 50  0000 C CNN "tolerance"
	1    4100 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5EBFC928
P 3650 3150
F 0 "R6" V 3550 3050 50  0000 C CNN
F 1 "8.2/10" V 3650 3200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" V 3580 3150 50  0001 C CNN
F 3 "~" H 3650 3150 50  0001 C CNN
F 4 "1%" V 3550 3250 50  0000 C CNN "tolerance"
	1    3650 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R R18
U 1 1 5EBFDC57
P 7050 5500
F 0 "R18" V 6950 5450 50  0000 L CNN
F 1 "2k" V 7050 5450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" V 6980 5500 50  0001 C CNN
F 3 "~" H 7050 5500 50  0001 C CNN
F 4 "1%" V 7150 5500 50  0000 C CNN "tolerance"
	1    7050 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R17
U 1 1 5EBFDCF7
P 7050 4850
F 0 "R17" V 6950 4800 50  0000 L CNN
F 1 "2k" V 7050 4800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" V 6980 4850 50  0001 C CNN
F 3 "~" H 7050 4850 50  0001 C CNN
F 4 "1%" V 7150 4850 50  0000 C CNN "tolerance"
	1    7050 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R16
U 1 1 5EC6E191
P 6250 5600
F 0 "R16" V 6150 5550 50  0000 L CNN
F 1 "240" V 6250 5500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" V 6180 5600 50  0001 C CNN
F 3 "~" H 6250 5600 50  0001 C CNN
F 4 "1%" V 6350 5550 50  0000 C CNN "tolerance"
	1    6250 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R20
U 1 1 5EC6E249
P 9750 5700
F 0 "R20" V 9650 5650 50  0000 L CNN
F 1 "240" V 9750 5600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" V 9680 5700 50  0001 C CNN
F 3 "~" H 9750 5700 50  0001 C CNN
F 4 "1%" V 9850 5650 50  0000 C CNN "tolerance"
	1    9750 5700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R22
U 1 1 5EC6E66E
P 10550 5600
F 0 "R22" V 10450 5550 50  0000 L CNN
F 1 "2k" V 10550 5550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" V 10480 5600 50  0001 C CNN
F 3 "~" H 10550 5600 50  0001 C CNN
F 4 "1%" V 10650 5600 50  0000 C CNN "tolerance"
	1    10550 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R21
U 1 1 5EC6E718
P 10550 4950
F 0 "R21" V 10450 4900 50  0000 L CNN
F 1 "2k" V 10550 4900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" V 10480 4950 50  0001 C CNN
F 3 "~" H 10550 4950 50  0001 C CNN
F 4 "1%" V 10650 4950 50  0000 C CNN "tolerance"
	1    10550 4950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5EE2CF80
P 10900 1100
F 0 "#PWR013" H 10900 850 50  0001 C CNN
F 1 "GND" V 10905 972 50  0000 R CNN
F 2 "" H 10900 1100 50  0001 C CNN
F 3 "" H 10900 1100 50  0001 C CNN
	1    10900 1100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10900 1100 10800 1100
Wire Wire Line
	9950 1100 9800 1100
Connection ~ 9800 1100
Wire Wire Line
	9800 1100 9800 850 
$Comp
L power:GND #PWR08
U 1 1 5EE9DC2C
P 7400 1000
F 0 "#PWR08" H 7400 750 50  0001 C CNN
F 1 "GND" V 7405 872 50  0000 R CNN
F 2 "" H 7400 1000 50  0001 C CNN
F 3 "" H 7400 1000 50  0001 C CNN
	1    7400 1000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7400 1000 7300 1000
Wire Wire Line
	6450 1000 6300 1000
Connection ~ 6300 1000
Wire Wire Line
	6300 1000 6300 800 
Connection ~ 2900 4550
Connection ~ 3150 5300
$Comp
L Device:C C2
U 1 1 5EFBBCD8
P 3400 4950
F 0 "C2" V 3450 5000 50  0000 L CNN
F 1 "100nF" V 3450 4650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3438 4800 50  0001 C CNN
F 3 "~" H 3400 4950 50  0001 C CNN
	1    3400 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 4800 3400 4550
Connection ~ 3400 4550
Wire Wire Line
	3400 4550 3700 4550
Wire Wire Line
	3400 5100 3400 5300
Connection ~ 3400 5300
Wire Wire Line
	3400 5300 3700 5300
Wire Notes Line
	3550 3850 3550 5550
Wire Notes Line
	3550 5550 4250 5550
Wire Notes Line
	4250 5550 4250 5350
Wire Notes Line
	4250 5350 4650 5350
Wire Notes Line
	4650 5350 4650 3850
Wire Notes Line
	4650 3850 3550 3850
Text Notes 3500 5650 0    50   ~ 0
place close to cpu
$Comp
L power:GND #PWR02
U 1 1 5F069FFA
P 3150 5300
F 0 "#PWR02" H 3150 5050 50  0001 C CNN
F 1 "GND" H 3155 5127 50  0000 C CNN
F 2 "" H 3150 5300 50  0001 C CNN
F 3 "" H 3150 5300 50  0001 C CNN
	1    3150 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5F06A0F3
P 6650 5800
F 0 "#PWR06" H 6650 5550 50  0001 C CNN
F 1 "GND" H 6655 5627 50  0000 C CNN
F 2 "" H 6650 5800 50  0001 C CNN
F 3 "" H 6650 5800 50  0001 C CNN
	1    6650 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 5800 7050 5800
Wire Wire Line
	7050 5650 7050 5800
Connection ~ 7050 5800
Wire Wire Line
	10150 5900 10550 5900
$Comp
L power:GND #PWR011
U 1 1 5F152728
P 10150 5900
F 0 "#PWR011" H 10150 5650 50  0001 C CNN
F 1 "GND" H 10155 5727 50  0000 C CNN
F 2 "" H 10150 5900 50  0001 C CNN
F 3 "" H 10150 5900 50  0001 C CNN
	1    10150 5900
	1    0    0    -1  
$EndComp
Text Label 3150 4550 2    50   ~ 0
cpu_ddr_vref
Text Label 6450 5150 2    50   ~ 0
ddr_vref1
Text Label 10000 5250 2    50   ~ 0
ddr_vref2
Text Notes 1150 6000 0    50   ~ 0
NOTE:\n1. vref is 20 mils wide at least
Wire Wire Line
	3300 3950 3300 2550
Wire Wire Line
	2600 3950 3300 3950
Wire Wire Line
	3250 3850 3250 2450
Wire Wire Line
	2600 3850 3250 3850
Wire Wire Line
	3200 3750 3200 2300
Wire Wire Line
	2600 3750 3200 3750
Wire Wire Line
	3150 3650 3150 2200
Wire Wire Line
	2600 3650 3150 3650
Wire Wire Line
	4450 3150 4550 3150
Connection ~ 4450 3150
Wire Wire Line
	3100 3550 3100 2050
Wire Wire Line
	2600 3550 3100 3550
Wire Wire Line
	3050 3450 3050 1950
Wire Wire Line
	2600 3450 3050 3450
Wire Wire Line
	3000 3350 3000 1800
Wire Wire Line
	2600 3350 3000 3350
Wire Wire Line
	2950 3250 2950 1700
Text Notes 4350 2550 0    50   ~ 0
diff
Text Notes 4350 2300 0    50   ~ 0
diff
Text Notes 4350 2050 0    50   ~ 0
diff
Text Notes 4350 1800 0    50   ~ 0
diff
Wire Notes Line
	7550 6050 7550 4450
Wire Notes Line
	7550 4450 6550 4450
Wire Notes Line
	11050 6150 11050 4550
Wire Notes Line
	11050 4550 10050 4550
Wire Notes Line
	6550 6050 7550 6050
Wire Notes Line
	10050 6150 11050 6150
Text Notes 7200 6900 0    157  ~ 31
ddr memory
Text Notes 4850 3450 2    50   ~ 0
diff
Wire Wire Line
	4550 3150 4550 3300
Wire Wire Line
	4550 3300 4850 3300
Wire Wire Line
	4450 3650 4550 3650
Wire Wire Line
	4550 3650 4550 3550
Wire Wire Line
	4550 3550 4850 3550
Connection ~ 4450 3650
$Comp
L Device:C C16
U 1 1 6044BE0B
P 6600 1000
F 0 "C16" V 6852 1000 50  0000 C CNN
F 1 "22uF" V 6761 1000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6638 850 50  0001 C CNN
F 3 "~" H 6600 1000 50  0001 C CNN
	1    6600 1000
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C38
U 1 1 60516966
P 10100 1100
F 0 "C38" V 10352 1100 50  0000 C CNN
F 1 "22uF" V 10261 1100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10138 950 50  0001 C CNN
F 3 "~" H 10100 1100 50  0001 C CNN
	1    10100 1100
	0    -1   -1   0   
$EndComp
Wire Notes Line rgb(194, 154, 0)
	4500 1900 4500 2100
Wire Notes Line rgb(194, 154, 0)
	4500 2100 4350 2100
Wire Notes Line rgb(194, 154, 0)
	4350 2100 4350 1900
Wire Notes Line rgb(194, 154, 0)
	4500 1900 4350 1900
Wire Notes Line rgb(194, 154, 0)
	4500 1650 4500 1850
Wire Notes Line rgb(194, 154, 0)
	4500 1850 4350 1850
Wire Notes Line rgb(194, 154, 0)
	4350 1850 4350 1650
Wire Notes Line rgb(194, 154, 0)
	4500 1650 4350 1650
Wire Notes Line rgb(194, 154, 0)
	4500 2150 4500 2350
Wire Notes Line rgb(194, 154, 0)
	4500 2350 4350 2350
Wire Notes Line rgb(194, 154, 0)
	4350 2350 4350 2150
Wire Notes Line rgb(194, 154, 0)
	4500 2150 4350 2150
Wire Notes Line rgb(194, 154, 0)
	4500 2400 4500 2600
Wire Notes Line rgb(194, 154, 0)
	4500 2600 4350 2600
Wire Notes Line rgb(194, 154, 0)
	4350 2600 4350 2400
Wire Notes Line rgb(194, 154, 0)
	4500 2400 4350 2400
Wire Notes Line rgb(194, 154, 0)
	4900 3200 4900 3600
Wire Notes Line rgb(194, 154, 0)
	4900 3600 4650 3600
Wire Notes Line rgb(194, 154, 0)
	4650 3600 4650 3200
Wire Notes Line rgb(194, 154, 0)
	4900 3200 4650 3200
$Comp
L Device:C C19
U 1 1 60D93C7F
P 6950 1150
F 0 "C19" V 6900 1250 50  0000 C CNN
F 1 "0.1uF" V 7000 1300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6988 1000 50  0001 C CNN
F 3 "~" H 6950 1150 50  0001 C CNN
	1    6950 1150
	0    1    1    0   
$EndComp
$Comp
L Device:C C8
U 1 1 60E5FD95
P 6550 1250
F 0 "C8" V 6500 1350 50  0000 C CNN
F 1 "0.1uF" V 6600 1400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6588 1100 50  0001 C CNN
F 3 "~" H 6550 1250 50  0001 C CNN
	1    6550 1250
	0    1    1    0   
$EndComp
$Comp
L Device:C C20
U 1 1 60EA3BD3
P 6950 1350
F 0 "C20" V 6900 1450 50  0000 C CNN
F 1 "0.1uF" V 7000 1500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6988 1200 50  0001 C CNN
F 3 "~" H 6950 1350 50  0001 C CNN
	1    6950 1350
	0    1    1    0   
$EndComp
$Comp
L Device:C C9
U 1 1 60EA3C53
P 6550 1450
F 0 "C9" V 6500 1550 50  0000 C CNN
F 1 "0.1uF" V 6600 1600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6588 1300 50  0001 C CNN
F 3 "~" H 6550 1450 50  0001 C CNN
	1    6550 1450
	0    1    1    0   
$EndComp
$Comp
L Device:C C21
U 1 1 60EA3CCF
P 6950 1550
F 0 "C21" V 6900 1650 50  0000 C CNN
F 1 "0.1uF" V 7000 1700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6988 1400 50  0001 C CNN
F 3 "~" H 6950 1550 50  0001 C CNN
	1    6950 1550
	0    1    1    0   
$EndComp
$Comp
L Device:C C10
U 1 1 60EA3D4B
P 6550 1650
F 0 "C10" V 6500 1750 50  0000 C CNN
F 1 "0.1uF" V 6600 1800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6588 1500 50  0001 C CNN
F 3 "~" H 6550 1650 50  0001 C CNN
	1    6550 1650
	0    1    1    0   
$EndComp
$Comp
L Device:C C22
U 1 1 60EA3DDB
P 6950 1750
F 0 "C22" V 6900 1850 50  0000 C CNN
F 1 "0.1uF" V 7000 1900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6988 1600 50  0001 C CNN
F 3 "~" H 6950 1750 50  0001 C CNN
	1    6950 1750
	0    1    1    0   
$EndComp
$Comp
L Device:C C11
U 1 1 60EA3E5D
P 6550 1850
F 0 "C11" V 6500 1950 50  0000 C CNN
F 1 "0.1uF" V 6600 2000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6588 1700 50  0001 C CNN
F 3 "~" H 6550 1850 50  0001 C CNN
	1    6550 1850
	0    1    1    0   
$EndComp
$Comp
L Device:C C23
U 1 1 60EA3EE1
P 6950 1950
F 0 "C23" V 6900 2050 50  0000 C CNN
F 1 "0.1uF" V 7000 2100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6988 1800 50  0001 C CNN
F 3 "~" H 6950 1950 50  0001 C CNN
	1    6950 1950
	0    1    1    0   
$EndComp
$Comp
L Device:C C12
U 1 1 60EA3F6D
P 6550 2050
F 0 "C12" V 6500 2150 50  0000 C CNN
F 1 "0.1uF" V 6600 2200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6588 1900 50  0001 C CNN
F 3 "~" H 6550 2050 50  0001 C CNN
	1    6550 2050
	0    1    1    0   
$EndComp
$Comp
L Device:C C24
U 1 1 60EA3FF9
P 6950 2150
F 0 "C24" V 6900 2250 50  0000 C CNN
F 1 "0.1uF" V 7000 2300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6988 2000 50  0001 C CNN
F 3 "~" H 6950 2150 50  0001 C CNN
	1    6950 2150
	0    1    1    0   
$EndComp
$Comp
L Device:C C13
U 1 1 60EA4083
P 6550 2250
F 0 "C13" V 6500 2350 50  0000 C CNN
F 1 "0.1uF" V 6600 2400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6588 2100 50  0001 C CNN
F 3 "~" H 6550 2250 50  0001 C CNN
	1    6550 2250
	0    1    1    0   
$EndComp
$Comp
L Device:C C25
U 1 1 60EA411B
P 6950 2350
F 0 "C25" V 6900 2450 50  0000 C CNN
F 1 "0.1uF" V 7000 2500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6988 2200 50  0001 C CNN
F 3 "~" H 6950 2350 50  0001 C CNN
	1    6950 2350
	0    1    1    0   
$EndComp
$Comp
L Device:C C14
U 1 1 60EA41A7
P 6550 2450
F 0 "C14" V 6500 2550 50  0000 C CNN
F 1 "0.1uF" V 6600 2600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6588 2300 50  0001 C CNN
F 3 "~" H 6550 2450 50  0001 C CNN
	1    6550 2450
	0    1    1    0   
$EndComp
$Comp
L Device:C C26
U 1 1 60EE8007
P 6950 2550
F 0 "C26" V 6900 2650 50  0000 C CNN
F 1 "0.1uF" V 7000 2700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6988 2400 50  0001 C CNN
F 3 "~" H 6950 2550 50  0001 C CNN
	1    6950 2550
	0    1    1    0   
$EndComp
$Comp
L Device:C C15
U 1 1 60EE80A9
P 6550 2650
F 0 "C15" V 6500 2750 50  0000 C CNN
F 1 "0.1uF" V 6600 2800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6588 2500 50  0001 C CNN
F 3 "~" H 6550 2650 50  0001 C CNN
	1    6550 2650
	0    1    1    0   
$EndComp
$Comp
L Device:C C27
U 1 1 60EE813D
P 6950 2750
F 0 "C27" V 6900 2850 50  0000 C CNN
F 1 "0.1uF" V 7000 2900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6988 2600 50  0001 C CNN
F 3 "~" H 6950 2750 50  0001 C CNN
	1    6950 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	6800 2750 6300 2750
Connection ~ 6300 2750
Wire Wire Line
	6400 2650 6300 2650
Wire Wire Line
	6300 2550 6800 2550
Wire Wire Line
	6400 2450 6300 2450
Wire Wire Line
	6300 2350 6800 2350
Wire Wire Line
	6400 2250 6300 2250
Wire Wire Line
	6300 2150 6800 2150
Wire Wire Line
	6400 2050 6300 2050
Wire Wire Line
	6300 1950 6800 1950
Wire Wire Line
	6400 1850 6300 1850
Wire Wire Line
	6300 1750 6800 1750
Wire Wire Line
	6400 1650 6300 1650
Wire Wire Line
	6300 1550 6800 1550
Wire Wire Line
	6400 1450 6300 1450
Wire Wire Line
	6300 1350 6800 1350
Wire Wire Line
	6300 1250 6400 1250
Wire Wire Line
	6300 1150 6800 1150
Wire Wire Line
	7100 1150 7300 1150
Wire Wire Line
	7300 1150 7300 1000
Connection ~ 7300 1000
Wire Wire Line
	7300 1000 6750 1000
Wire Wire Line
	7100 1350 7300 1350
Wire Wire Line
	7300 1350 7300 1250
Connection ~ 7300 1150
Wire Wire Line
	7100 1550 7300 1550
Wire Wire Line
	7300 1550 7300 1450
Connection ~ 7300 1350
Wire Wire Line
	7100 1750 7300 1750
Wire Wire Line
	7300 1750 7300 1650
Connection ~ 7300 1550
Wire Wire Line
	7100 1950 7300 1950
Wire Wire Line
	7300 1950 7300 1850
Connection ~ 7300 1750
Wire Wire Line
	7100 2150 7300 2150
Wire Wire Line
	7300 2150 7300 2050
Connection ~ 7300 1950
Wire Wire Line
	7100 2350 7300 2350
Wire Wire Line
	7300 2350 7300 2250
Connection ~ 7300 2150
Wire Wire Line
	7100 2550 7300 2550
Wire Wire Line
	7300 2550 7300 2450
Connection ~ 7300 2350
Wire Wire Line
	7100 2750 7300 2750
Wire Wire Line
	7300 2750 7300 2650
Connection ~ 7300 2550
Wire Wire Line
	6700 2650 7300 2650
Connection ~ 7300 2650
Wire Wire Line
	7300 2650 7300 2550
Wire Wire Line
	6700 2450 7300 2450
Connection ~ 7300 2450
Wire Wire Line
	7300 2450 7300 2350
Wire Wire Line
	6700 2250 7300 2250
Connection ~ 7300 2250
Wire Wire Line
	7300 2250 7300 2150
Wire Wire Line
	6700 2050 7300 2050
Connection ~ 7300 2050
Wire Wire Line
	7300 2050 7300 1950
Wire Wire Line
	6700 1850 7300 1850
Connection ~ 7300 1850
Wire Wire Line
	7300 1850 7300 1750
Wire Wire Line
	6700 1650 7300 1650
Connection ~ 7300 1650
Wire Wire Line
	7300 1650 7300 1550
Wire Wire Line
	6700 1450 7300 1450
Connection ~ 7300 1450
Wire Wire Line
	7300 1450 7300 1350
Wire Wire Line
	6700 1250 7300 1250
Connection ~ 7300 1250
Wire Wire Line
	7300 1250 7300 1150
$Comp
L Device:C C41
U 1 1 61913D4F
P 10450 1250
F 0 "C41" V 10400 1350 50  0000 C CNN
F 1 "0.1uF" V 10500 1400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 1100 50  0001 C CNN
F 3 "~" H 10450 1250 50  0001 C CNN
	1    10450 1250
	0    1    1    0   
$EndComp
$Comp
L Device:C C30
U 1 1 61913D56
P 10050 1350
F 0 "C30" V 10000 1450 50  0000 C CNN
F 1 "0.1uF" V 10100 1500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10088 1200 50  0001 C CNN
F 3 "~" H 10050 1350 50  0001 C CNN
	1    10050 1350
	0    1    1    0   
$EndComp
$Comp
L Device:C C42
U 1 1 61913D5D
P 10450 1450
F 0 "C42" V 10400 1550 50  0000 C CNN
F 1 "0.1uF" V 10500 1600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 1300 50  0001 C CNN
F 3 "~" H 10450 1450 50  0001 C CNN
	1    10450 1450
	0    1    1    0   
$EndComp
$Comp
L Device:C C31
U 1 1 61913D64
P 10050 1550
F 0 "C31" V 10000 1650 50  0000 C CNN
F 1 "0.1uF" V 10100 1700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10088 1400 50  0001 C CNN
F 3 "~" H 10050 1550 50  0001 C CNN
	1    10050 1550
	0    1    1    0   
$EndComp
$Comp
L Device:C C43
U 1 1 61913D6B
P 10450 1650
F 0 "C43" V 10400 1750 50  0000 C CNN
F 1 "0.1uF" V 10500 1800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 1500 50  0001 C CNN
F 3 "~" H 10450 1650 50  0001 C CNN
	1    10450 1650
	0    1    1    0   
$EndComp
$Comp
L Device:C C32
U 1 1 61913D72
P 10050 1750
F 0 "C32" V 10000 1850 50  0000 C CNN
F 1 "0.1uF" V 10100 1900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10088 1600 50  0001 C CNN
F 3 "~" H 10050 1750 50  0001 C CNN
	1    10050 1750
	0    1    1    0   
$EndComp
$Comp
L Device:C C44
U 1 1 61913D79
P 10450 1850
F 0 "C44" V 10400 1950 50  0000 C CNN
F 1 "0.1uF" V 10500 2000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 1700 50  0001 C CNN
F 3 "~" H 10450 1850 50  0001 C CNN
	1    10450 1850
	0    1    1    0   
$EndComp
$Comp
L Device:C C33
U 1 1 61913D80
P 10050 1950
F 0 "C33" V 10000 2050 50  0000 C CNN
F 1 "0.1uF" V 10100 2100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10088 1800 50  0001 C CNN
F 3 "~" H 10050 1950 50  0001 C CNN
	1    10050 1950
	0    1    1    0   
$EndComp
$Comp
L Device:C C45
U 1 1 61913D87
P 10450 2050
F 0 "C45" V 10400 2150 50  0000 C CNN
F 1 "0.1uF" V 10500 2200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 1900 50  0001 C CNN
F 3 "~" H 10450 2050 50  0001 C CNN
	1    10450 2050
	0    1    1    0   
$EndComp
$Comp
L Device:C C34
U 1 1 61913D8E
P 10050 2150
F 0 "C34" V 10000 2250 50  0000 C CNN
F 1 "0.1uF" V 10100 2300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10088 2000 50  0001 C CNN
F 3 "~" H 10050 2150 50  0001 C CNN
	1    10050 2150
	0    1    1    0   
$EndComp
$Comp
L Device:C C46
U 1 1 61913D95
P 10450 2250
F 0 "C46" V 10400 2350 50  0000 C CNN
F 1 "0.1uF" V 10500 2400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 2100 50  0001 C CNN
F 3 "~" H 10450 2250 50  0001 C CNN
	1    10450 2250
	0    1    1    0   
$EndComp
$Comp
L Device:C C35
U 1 1 61913D9C
P 10050 2350
F 0 "C35" V 10000 2450 50  0000 C CNN
F 1 "0.1uF" V 10100 2500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10088 2200 50  0001 C CNN
F 3 "~" H 10050 2350 50  0001 C CNN
	1    10050 2350
	0    1    1    0   
$EndComp
$Comp
L Device:C C47
U 1 1 61913DA3
P 10450 2450
F 0 "C47" V 10400 2550 50  0000 C CNN
F 1 "0.1uF" V 10500 2600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 2300 50  0001 C CNN
F 3 "~" H 10450 2450 50  0001 C CNN
	1    10450 2450
	0    1    1    0   
$EndComp
$Comp
L Device:C C36
U 1 1 61913DAA
P 10050 2550
F 0 "C36" V 10000 2650 50  0000 C CNN
F 1 "0.1uF" V 10100 2700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10088 2400 50  0001 C CNN
F 3 "~" H 10050 2550 50  0001 C CNN
	1    10050 2550
	0    1    1    0   
$EndComp
$Comp
L Device:C C48
U 1 1 61913DB1
P 10450 2650
F 0 "C48" V 10400 2750 50  0000 C CNN
F 1 "0.1uF" V 10500 2800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 2500 50  0001 C CNN
F 3 "~" H 10450 2650 50  0001 C CNN
	1    10450 2650
	0    1    1    0   
$EndComp
$Comp
L Device:C C37
U 1 1 61913DB8
P 10050 2750
F 0 "C37" V 10000 2850 50  0000 C CNN
F 1 "0.1uF" V 10100 2900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10088 2600 50  0001 C CNN
F 3 "~" H 10050 2750 50  0001 C CNN
	1    10050 2750
	0    1    1    0   
$EndComp
$Comp
L Device:C C49
U 1 1 61913DBF
P 10450 2850
F 0 "C49" V 10400 2950 50  0000 C CNN
F 1 "0.1uF" V 10500 3000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10488 2700 50  0001 C CNN
F 3 "~" H 10450 2850 50  0001 C CNN
	1    10450 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	10300 2850 9800 2850
Wire Wire Line
	9900 2750 9800 2750
Wire Wire Line
	9800 2650 10300 2650
Wire Wire Line
	9900 2550 9800 2550
Wire Wire Line
	9800 2450 10300 2450
Wire Wire Line
	9900 2350 9800 2350
Wire Wire Line
	9800 2250 10300 2250
Wire Wire Line
	9900 2150 9800 2150
Wire Wire Line
	9800 2050 10300 2050
Wire Wire Line
	9900 1950 9800 1950
Wire Wire Line
	9800 1850 10300 1850
Wire Wire Line
	9900 1750 9800 1750
Wire Wire Line
	9800 1650 10300 1650
Wire Wire Line
	9900 1550 9800 1550
Wire Wire Line
	9800 1450 10300 1450
Wire Wire Line
	9800 1350 9900 1350
Wire Wire Line
	9800 1250 10300 1250
Wire Wire Line
	10600 1250 10800 1250
Wire Wire Line
	10800 1250 10800 1100
Wire Wire Line
	10600 1450 10800 1450
Wire Wire Line
	10800 1450 10800 1350
Connection ~ 10800 1250
Wire Wire Line
	10600 1650 10800 1650
Wire Wire Line
	10800 1650 10800 1550
Connection ~ 10800 1450
Wire Wire Line
	10600 1850 10800 1850
Wire Wire Line
	10800 1850 10800 1750
Connection ~ 10800 1650
Wire Wire Line
	10600 2050 10800 2050
Wire Wire Line
	10800 2050 10800 1950
Connection ~ 10800 1850
Wire Wire Line
	10600 2250 10800 2250
Wire Wire Line
	10800 2250 10800 2150
Connection ~ 10800 2050
Wire Wire Line
	10600 2450 10800 2450
Wire Wire Line
	10800 2450 10800 2350
Connection ~ 10800 2250
Wire Wire Line
	10600 2650 10800 2650
Wire Wire Line
	10800 2650 10800 2550
Connection ~ 10800 2450
Wire Wire Line
	10600 2850 10800 2850
Wire Wire Line
	10800 2850 10800 2750
Connection ~ 10800 2650
Wire Wire Line
	10200 2750 10800 2750
Connection ~ 10800 2750
Wire Wire Line
	10800 2750 10800 2650
Wire Wire Line
	10200 2550 10800 2550
Connection ~ 10800 2550
Wire Wire Line
	10800 2550 10800 2450
Wire Wire Line
	10200 2350 10800 2350
Connection ~ 10800 2350
Wire Wire Line
	10800 2350 10800 2250
Wire Wire Line
	10200 2150 10800 2150
Connection ~ 10800 2150
Wire Wire Line
	10800 2150 10800 2050
Wire Wire Line
	10200 1950 10800 1950
Connection ~ 10800 1950
Wire Wire Line
	10800 1950 10800 1850
Wire Wire Line
	10200 1750 10800 1750
Connection ~ 10800 1750
Wire Wire Line
	10800 1750 10800 1650
Wire Wire Line
	10200 1550 10800 1550
Connection ~ 10800 1550
Wire Wire Line
	10800 1550 10800 1450
Wire Wire Line
	10200 1350 10800 1350
Connection ~ 10800 1350
Wire Wire Line
	10800 1350 10800 1250
Connection ~ 10800 1100
Wire Wire Line
	10800 1100 10250 1100
Connection ~ 9800 2850
Text GLabel 6300 800  0    50   Input ~ 0
vdd_mem
Text GLabel 9800 850  0    50   Input ~ 0
vdd_mem
Text GLabel 4350 3950 2    50   Input ~ 0
vdd_mem
Text GLabel 7300 4550 2    50   Input ~ 0
vdd_mem
Text GLabel 10800 4650 2    50   Input ~ 0
vdd_mem
Text Notes 2300 750  0    50   ~ 10
all lines from cpu to memory must be distance-matched
Text Notes 6750 4350 0    50   ~ 0
place close to ram
Text Notes 10250 4450 0    50   ~ 0
place close to ram
Wire Notes Line
	6550 4450 6550 6050
Wire Notes Line
	10050 4550 10050 6150
Text Label 2900 4050 0    50   ~ 0
ck0+
Text Label 2900 4150 0    50   ~ 0
ck0-
Wire Notes Line
	9850 3700 10200 3700
Wire Notes Line
	6400 3600 6850 3600
Wire Wire Line
	3300 2550 4800 2550
Wire Wire Line
	3250 2450 4800 2450
Wire Wire Line
	3200 2300 4800 2300
Wire Wire Line
	3150 2200 4800 2200
Wire Wire Line
	3100 2050 4800 2050
Wire Wire Line
	3050 1950 4800 1950
Wire Wire Line
	3000 1800 4800 1800
Wire Wire Line
	2950 1700 4800 1700
Wire Wire Line
	3800 3150 4450 3150
Wire Wire Line
	3800 3650 4450 3650
$EndSCHEMATC
